*This application is a part of the Cafet Manager Software. Thereby, whthout contrary mention, it is licensed under the following license (fr)without being limited by:*

-------------

<style type="text/css">
    ol { list-style-type: lower-alpha; }
    ol ol { list-style-type: default; }
    ol ol ol { list-style-type: upper--alpha; }
</style>

# Gestion Cafet’ – License utilisateur 1.0
En exerçant les Droits de Licence (définis ci-après) Vous acceptez et reconnaissez être lié par les termes et conditions de cette licence (la « Licence »). Dans la mesure où cette licence peut être interprétée comme un contrat, les droits Vous sont accordés en échange de Votre acceptation dudit contrat, et le Propriétaire vous accorde ces droits en contrepartie des avantages qu’il reçoit du fait de rendre le Logiciel disponible selon ces termes et conditions.

## Article 1 : Définitions
01. **Contenu Dérivé** désigne un contenu soumis au Droit d'Auteur et aux Droits Similaires dérivé ou basé sur le Logiciel et dans lequel le Logiciel est traduit, modifié, arrangé, transformé ou modifié d'une manière nécessitant une autorisation en vertu des Droits d'Auteur et Droits Similaires par le Propriétaire.
02. **Droit d'Auteur et Droits Similaires** désigne un droit d'auteur et/ou des droits similaires étroitement liés au droit d'auteur, y compris, sans s'y limiter, la performance, la diffusion, l'enregistrement sonore et les Droits Sui Generis sans égard à la façon dont les droits sont étiquetés ou catégorisés. Aux fins de cette Licence, les droits spécifiés dans la Section 2 (b) (1) - (2) ne sont pas des droits d'auteur et des droits similaires.
03. **Mesures Technologiques Effectives** désigne les mesures qui, en l'absence d'autorité appropriée, ne peuvent être contournées en vertu des lois remplissant les obligations prévues à l'article 11 du Traité de l'OMPI sur le droit d'auteur adopté le 20 décembre 1996 et/ou des accords internationaux similaires.
04. **Exceptions et limitations** signifie un usage loyal, un traitement équitable et/ou toute autre exception ou limitation aux Droits d'Auteur et Droits Similaires qui s'appliquent à Votre utilisation du Logiciel.
05. **Logiciel** désigne, ici, le logiciel client, le serveur, l’API, la base de données ou tout autre matériel désigné comme soumis à la présente Licence.
06. **Droits de Licence** désigne les droits qui Vous sont accordés sous réserve des termes et conditions de cette Licence, qui sont limités à tous les Droits d'Auteur et Droits Similaires qui s'appliquent à Votre utilisation du Logiciel et que le Propriétaire est autorisé à accorder par une licence.
07. **Propriétaire** désigne Damien Djomby, individu qui concède des droits en vertu de cette Licence.
08. **Partager** signifie fournir du matériel au public par tout moyen ou processus qui nécessite une permission en vertu des Droits de Licence, tels que la reproduction, l'affichage public, l'exécution publique, la distribution, la diffusion, la communication ou l'importation, et rendre le matériel accessible au public de sorte que les membres du public peuvent accéder au matériel à partir d'un endroit et à un moment individuellement choisi par eux.
09. **Droits Sui Generis** désigne les droits de base de données autres que le droit d'auteur résultant de la directive 96/9/CE du Parlement européen et du Conseil du 11 mars 1996 relative à la protection juridique des bases de données, telles que modifiées et/ou abouties ainsi que d'autres droits essentiellement équivalents partout dans le monde.
10. **Vous** désigne la personne « morale » ou « physique » qui exerce les Droits Accordés par cette Licence. Votre a une signification similaire.

## Article 2 : Portée
1. Droits accordés par la Licence
    1. Sous réserve des termes et conditions de cette Licence, le Propriétaire Vous accorde par la présente une licence mondiale, libre de droits, non sous-licenciable, non exclusive et irrévocable pour exercer les Droits de Licence avec le Logiciel pour utiliser ce dernier dans son intégralité, c’est-à-dire toutes les fonctionnalités incluses.
    2. Pour éviter tout doute éventuel, lorsque des exceptions et limitations s'appliquent à Votre utilisation, cette Licence ne s'applique pas et vous n'avez pas besoin de vous conformer à ses termes et conditions.
    3. La durée de cette licence est spécifiée à la section 6.
    4. Le Propriétaire Vous autorise à exercer les Droits de Licence et à effectuer des améliorations du Logiciel pour Votre propre utilisation dans le respect des Mesures Technologiques Effectives si cette modification fait suite à une demande et à l’obtention de l’accord du Propriétaire puisque cela produit un Contenu Dérivé.
    5. Utilisation du Logiciel
        1. Tout détenteur d’une autorisation fournie par le Propriétaire reçoit automatiquement une offre du Propriétaire pour exercer les Droit de Licence conformément aux termes et conditions de cette Licence.
        1. Il ne Vous est pas possible de partager Votre propre version du logiciel et ne pouvez par conséquent pas créer de nouvelle licence.
    6. Aucune approbation. Rien dans cette licence ne constitue ou peut être considéré comme une permission pour affirmer que Vous êtes, ou que Votre utilisation du Logiciel est, connecté avec, sponsorisé, approuvé, ou autorisé par le Propriétaire pour recevoir l’attribution comme prévu à la section 3.a.1.A.i
2. Autres droits
    1. Les droits moraux, tels que le droit à l'intégrité, ne sont pas autorisés en vertu de cette Licence mais cette dernière ne vous interdit pas le droit de publicité dans la mesure où le Logiciel est cité

En cours d'édition..
