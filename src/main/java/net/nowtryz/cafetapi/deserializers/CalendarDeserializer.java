package net.nowtryz.cafetapi.deserializers;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Calendar.Builder;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class CalendarDeserializer implements JsonDeserializer<Calendar> {

	@Override
	public Calendar deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		JsonObject object = json.getAsJsonObject();

		int year = object.getAsJsonPrimitive("year").getAsInt();
		int month = object.getAsJsonPrimitive("month").getAsInt();
		int day = object.getAsJsonPrimitive("day").getAsInt();

		int hour = object.getAsJsonPrimitive("year").getAsInt();
		int mins = object.getAsJsonPrimitive("month").getAsInt();
		int secs = object.getAsJsonPrimitive("day").getAsInt();

		Builder cal = new Calendar.Builder();
		cal.setDate(year, month - 1, day);
		cal.setTimeOfDay(hour, mins, secs);
		return cal.build();
	}

}
