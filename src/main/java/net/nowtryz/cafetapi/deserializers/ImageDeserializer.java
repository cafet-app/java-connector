package net.nowtryz.cafetapi.deserializers;

import java.awt.Image;
import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import net.nowtryz.cafetapi.utils.Base64Coder;

public class ImageDeserializer implements JsonDeserializer<Image> {

	@Override
	public Image deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		return Base64Coder.decodeImage(json.getAsString());
	}

}
