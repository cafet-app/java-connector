package net.nowtryz.cafetapi.deserializers;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import net.nowtryz.cafetapi.datas.ExpenseDetail;
import net.nowtryz.cafetapi.datas.FormulaBought;
import net.nowtryz.cafetapi.datas.ProductBought;

public class ExpenseDetailDeserializer implements JsonDeserializer<ExpenseDetail> {

	@Override
	public ExpenseDetail deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
		JsonObject object = json.getAsJsonObject();
		String jsonType = object.getAsJsonPrimitive().getAsString();

		if (FormulaBought.class.getSimpleName().equalsIgnoreCase(jsonType))
			return context.deserialize(json, FormulaBought.class);
		else if (ProductBought.class.getSimpleName().equalsIgnoreCase(jsonType))
			return context.deserialize(json, ProductBought.class);

		return null;
	}

}
