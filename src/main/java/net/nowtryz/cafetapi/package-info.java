/**
 * Main package for the Cafet API, the API that connect the application to the server
 * @author Damien
 * @since API 1.0 (2018)
 */
package net.nowtryz.cafetapi;