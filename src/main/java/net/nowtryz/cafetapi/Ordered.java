package net.nowtryz.cafetapi;

import net.nowtryz.cafetapi.datas.Formula;
import net.nowtryz.cafetapi.datas.Product;

abstract class Ordered {
	public String type;
	
	static class ProductOrdered extends Ordered {
		private static final String PRODUCT = "product";
		public int id, amount;

		public ProductOrdered(Product product, int amount) {
			super.type = PRODUCT;
			this.id = product.getId();
			this.amount = amount;
		}

	}
	
	static class FormulaOrdered extends Ordered {
		private static final String FORMULA = "formula";
		public int id, amount, products[];

		public FormulaOrdered(Formula formula, int amount, Product[] products) {
			super.type = FORMULA;
			this.id = formula.getId();
			this.amount = amount;
			this.products = new int[products.length];
			
			for(int i = 0; i < products.length; i++) this.products[i] = products[i].getId();
		}
	}
}
