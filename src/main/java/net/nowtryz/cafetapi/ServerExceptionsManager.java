package net.nowtryz.cafetapi;

import com.google.gson.JsonSyntaxException;

import net.nowtryz.cafetapi.exceptions.APIException;
import net.nowtryz.cafetapi.exceptions.BannedUserException;
import net.nowtryz.cafetapi.exceptions.FailedAuthenticationException;
import net.nowtryz.cafetapi.exceptions.HTTPException;
import net.nowtryz.cafetapi.exceptions.IllegalResultException;
import net.nowtryz.cafetapi.exceptions.PermissionException;
import net.nowtryz.cafetapi.exceptions.ServerErrorException;
import net.nowtryz.cafetapi.http.HTTPResponse;
import net.nowtryz.cafetapi.parsers.ErrorParser;
import net.nowtryz.cafetapi.results.ErrorResult;

public class ServerExceptionsManager {

	/**
	 * Handle HTTP error from the server return more explicit exception
	 * @param e the exception to handle
	 */
	public static APIException handle(HTTPException e) {
		// FIXME do it!
		HTTPResponse response = e.getResponse();
		String body = response.getResponse();
		try {
			ErrorResult error = ErrorParser.parseResponse(response);
			
			switch (error.getErrorCode()) {
				case "02-001": return new FailedAuthenticationException(error.getErrorMessage(), e);
				case "02-002": return new BannedUserException(error.getErrorMessage(), e);
				case "02-003": return new PermissionException(error);
				default: return new ServerErrorException(error);
			}
		} catch(JsonSyntaxException ex) {
			return new IllegalResultException("unable to parse: " + body, ex);
		} catch (IllegalResultException ex) {
			return ex;
		}
	}

}
