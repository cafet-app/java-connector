/**
 * 
 */
package net.nowtryz.cafetapi.user;

import java.util.HashMap;

import net.nowtryz.cafetapi.utils.Lang;

/**
 * @author damie
 * @since API 1.0 (2018)
 */
public class User implements Permissible {
    private int id;
    private String pseudo;
    private String firstname;
    private String familyName;
    private String email;
    private String phone;
    private HashMap<String, Boolean> permissions;
    private Group group;
    
    public User(int id, String pseudo, String firstname, String name, String email, String phone, Group group, HashMap<String, Boolean> permissions) {
        this.id = id;
        this.pseudo = pseudo;
        this.firstname = firstname;
        this.familyName = name;
        this.email = email;
        this.phone = phone;
        this.group = group;
        this.permissions = permissions;
    }
    
    public User() {
    	id = 0;
    	pseudo = Lang.get("Unknown");
    	firstname = Lang.get("Unknown");
    	familyName = Lang.get("Unknown");
    	email = Lang.get("Unknown");
    	phone = "0";
    	group = new Group();
    	permissions = new HashMap<String, Boolean>();
    }

	/**
	 * Returns the id of the User
	 * @return the id
	 * @since API 1.0 (2018)
	 */
	public int getId() {
		return id;
	}

	/**
	 * Returns the pseudo of the User
	 * @return the pseudo
	 * @since API 1.0 (2018)
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * Returns the firstname of the User
	 * @return the firstname
	 * @since API 1.0 (2018)
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Returns the familyName of the User
	 * @return the familyName
	 * @since API 1.0 (2018)
	 */
	public String getName() {
		return familyName;
	}

	/**
	 * Returns the email of the User
	 * @return the email
	 * @since API 1.0 (2018)
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Returns the phone of the User
	 * @return the phone
	 * @since API 1.0 (2018)
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Returns the group of the User
	 * @return the group
	 * @since API 1.0 (2018)
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * Sets the group
	 * @param group the group to set
	 * @since API 1.0 (2018)
	 */
	public void setGroup(Group group) {
		this.group = group;
	}

	/* (non-Javadoc)
	 * @see net.nowtryz.cafetapi.user.Permissible#getPermissions()
	 * @since API 1.0 (2018)
	 */
	@Override
	public HashMap<String, Boolean> getPermissions() {
		return permissions;
	}
	
	@Override
	public String toString() {
		return "{"
				+ "\"id\":" + id + ","
				+ "\"pseudo\":\"" + pseudo + "\","
				+ "\"firstname\":\"" + firstname + "\","
				+ "\"familyName\":\"" + familyName + "\","
				+ "\"email\":\"" + email + "\","
				+ "\"phone\":\"" + phone + "\","
				+ "\"group\":" + group + ","
				+ "\"permissions\":" + permissions
			+ "}";
	}

}
