package net.nowtryz.cafetapi.user;

import java.util.HashMap;

public interface Permissible {
    
	/**
     * Return wether the member has the given permission or not
     * @param permission the permission to check
     * @return bool the permission check
     * @since API 1.0.0 (2018)
     */
    public default boolean hasPermission(String permission) {
    	return Permissions.checkPermission(permission, this);
    }
    
    /**
     * Return every permission witch the member is liable from
     * @return HashMap<String,Boolean> an {@linkplain HashMap} of 
     * @since API 1.0.0 (2018)
     */
    public HashMap<String,Boolean> getPermissions();
}
