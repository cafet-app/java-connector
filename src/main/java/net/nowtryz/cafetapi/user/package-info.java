/**
 * User infos and permessions package
 * @author damie
 * @since API 1.0 (2018)
 */
package net.nowtryz.cafetapi.user;