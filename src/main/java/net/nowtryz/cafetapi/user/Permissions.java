package net.nowtryz.cafetapi.user;

import java.util.HashMap;
import java.util.Set;

public enum Permissions {
	//ALL (*)
    ALL("*"),
    
    //GLOBAL
    GLOBAL("global"),
    GLOBAL_HIDDEN("global_hidden"),
    GLOBAL_UNTOUCHABLE("global_untouchable"),
    GLOBAL_CONNECT("global_connect"),
    
    //SITE
    SITE("site"),
    SITE_POST("site_post"),
    SITE_MANAGE("site_manage"),
    
    //CAFET
    CAFET("cafet"),
    CAFET_PURCHASE("cafet_pruchase"),
    CAFET_ADMIN("cafet_admin"),
    CAFET_ADMIN_PANELACCESS("cafet_admin_panelaccess"),
    CAFET_ADMIN_ORDER("cafet_admin_order"),
    CAFET_ADMIN_RELOAD("cafet_admin_reload"),
    CAFET_ADMIN_NEGATIVERELOAD("cafet_admin_negativereload"),
    CAFET_ADMIN_STATS("cafet_admin_stats"),
    CAFET_ADMIN_GET("cafet_admin_get"),
    CAFET_ADMIN_GET_CLIENTS("cafet_admin_get_clients"),
    CAFET_ADMIN_GET_RELOADS("cafet_admin_get_reloads"),
    CAFET_ADMIN_GET_EXPENSES("cafet_admin_get_expenses"),
    CAFET_ADMIN_GET_PRODUCTS("cafet_admin_get_products"),
    CAFET_ADMIN_GET_FORMULAS("cafet_admin_get_formulas"),
    CAFET_ADMIN_MANAGE("cafet_admin_manage"),
    CAFET_ADMIN_MANAGE_PRODUCTS("cafet_admin_manage_products"),
    CAFET_ADMIN_MANAGE_FORMULAS("cafet_admin_manage_formulas"),
    CAFET_ADMIN_MANAGE_SETTINGS("cafet_admin_manage_settings"),
    CAFET_ADMIN_MANAGE_STOCKS("cafet_admin_manage_stocks");
	
	private String value;
	
	private Permissions(String value) {
		this.value = value;
	}

	/**
	 * Returns the permission of the Permissions
	 * @return the permission
	 * @since API 1.0 (2018)
	 */
	public String value() {
		return value;
	}
	
	public static boolean checkPermission(String permission, Permissible permissible) {
		String[] path;
		String check = "";
		
		HashMap<String, Boolean> permissions = permissible.getPermissions();
		Set<String> definedPermissions = permissions.keySet();
		boolean result = definedPermissions.contains(ALL.value()) ? permissions.get(ALL.value()) : false;
		
		if(permission == "" && !result) return false;
		
		path = permission.split("_");
		
		for(int i=0; i < path.length; i++) {
			check += path[i];
			
			if(definedPermissions.contains(check)) result = permissions.get(check);
			
			check += "_";
		}
		
		return result;
	}
}
