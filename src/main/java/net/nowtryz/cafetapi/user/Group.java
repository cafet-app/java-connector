package net.nowtryz.cafetapi.user;

import java.util.HashMap;

import net.nowtryz.cafetapi.utils.Lang;

public class Group implements Permissible {
    private String name;
    private HashMap<String, Boolean> permissions;

    /**
     * 
     * @param name
     * @param permissions
     * @since API 1.0 (2018)
     */
    public Group(String name, HashMap<String, Boolean> permissions) {
        this.name = name;
        this.permissions = permissions;
    }
    public Group() {
    	name = Lang.get("Unknown");
    	permissions = new HashMap<String, Boolean>();
    }

	/**
	 * Returns the name of the Group
	 * @return the name
	 * @since API 1.0.0 (2018)
	 */
	public String getName() {
		return name;
	}

	@Override
	public HashMap<String, Boolean> getPermissions() {
		return this.permissions;
	}

	@Override
	public String toString() {
		return "{"
				+ "\"name\":\"" + name + "\","
				+ "\"permissions\":" + permissions
			+ "}";
	}
}
