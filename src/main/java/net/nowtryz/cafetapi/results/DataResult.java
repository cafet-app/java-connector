package net.nowtryz.cafetapi.results;

import net.nowtryz.cafetapi.datas.CafetData;

public class DataResult<T extends CafetData> extends Result {
	private T data;

	/**
	 * 
	 * @param status
	 * @param computing
	 * @param parsed
	 * @since API 1.0 (2018)
	 */
	public DataResult(Status status, float computing, T parsed) {
		super(status, computing);
		this.data = parsed;
	}

	/**
	 * Returns the data of the DataResult
	 * @return the data
	 * @since API 1.0 (2018)
	 */
	public T getData() {
		return data;
	}
	
}
