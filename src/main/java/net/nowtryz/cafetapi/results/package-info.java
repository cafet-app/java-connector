/**
 * Results managing system
 * @author Damien
 * @since API 1.0 (2018)
 */
package net.nowtryz.cafetapi.results;