package net.nowtryz.cafetapi.results;

import net.nowtryz.cafetapi.datas.CafetData;

public class ArrayResult<T extends CafetData> extends Result {
	private T[] datas;

	/**
	 * 
	 * @param status
	 * @param computing
	 * @param parsed
	 * @since API 1.0 (2018)
	 */
	public ArrayResult(Status status, float computing, T[] parsed) {
		super(status, computing);
		this.datas = parsed;
	}

	/**
	 * Returns the datas of the ArrayResult
	 * @return the datas
	 * @since API 1.0 (2018)
	 */
	public T[] getDatas() {
		return datas;
	}
	
}