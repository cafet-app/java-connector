package net.nowtryz.cafetapi.results;

public class ErrorResult extends Result {
	private String error_code;
	private String error_type;
	private String error_message;
	private String additional_message = null;
	
	/**
	 * 
	 * @param status
	 * @param computing
	 * @param error_code
	 * @param error_type
	 * @param error_message
	 * @param additional_message
	 * @since API 1.0 (2018)
	 */
	public ErrorResult(Status status, float computing, String error_code, String error_type, String error_message, String additional_message) {
		super(status, computing);
		this.error_code = error_code;
		this.error_type = error_type;
		this.error_message = error_message;
		this.additional_message = additional_message;
	}

	/**
	 * Returns the error_code of the ErrorResult
	 * @return the error_code
	 * @since API 1.0 (2018)
	 */
	public String getErrorCode() {
		return error_code;
	}

	/**
	 * Returns the error_type of the ErrorResult
	 * @return the error_type
	 * @since API 1.0 (2018)
	 */
	public String getErrorType() {
		return error_type;
	}

	/**
	 * Returns the error_message of the ErrorResult
	 * @return the error_message
	 * @since API 1.0 (2018)
	 */
	public String getErrorMessage() {
		return error_message;
	}

	/**
	 * Returns the additional_message of the ErrorResult
	 * @return the additional_message
	 * @since API 1.0 (2018)
	 */
	public final String getAdditionalMessage() {
		return additional_message;
	}

	public final boolean hasAdditionalMessage() {
		return additional_message != null;
	}
}
