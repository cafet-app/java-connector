package net.nowtryz.cafetapi.results;

public class Result {
	private Status status;
	private float computing;
	
	public Result(Status status, float computing) {
		this.status = status;
		this.computing = computing;
	}
	
	/**
	 * Returns the status of the Result
	 * @return the status
	 * @since API 1.0 (2018)
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * Returns the computing of the Result
	 * @return the computing
	 * @since API 1.0 (2018)
	 */
	public float getComputing() {
		return computing;
	}
}
