package net.nowtryz.cafetapi.results;

import net.nowtryz.cafetapi.user.User;

/**
 * @author damie
 * @since API 1.0 (2018)
 */
public class LoginResult extends Result {
	private String sessionId;
	private User user;

	/**
	 * @param status
	 * @param computing
	 * @since API 1.0 (2018)
	 */
	public LoginResult(Status status, float computing, String sessionId, User user) {
		super(status, computing);
		
		this.sessionId = sessionId;
		this.user = user;
	}

	/**
	 * Returns the sessionId of the LoginResult
	 * @return the sessionId
	 * @since API 1.0 (2018)
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Returns the serverVersion of the LoginResult
	 * @return the serverVersion
	 * @since API 1.0 (2018)
	 */
	@Deprecated
	public String getServerVersion() {
		return null;
	}

	/**
	 * Returns the user of the LoginResult
	 * @return the user
	 * @since API 1.0 (2018)
	 */
	public User getUser() {
		return user;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LoginResult [sessionId=" + sessionId + ", user=" + user + "]";
	}

}
