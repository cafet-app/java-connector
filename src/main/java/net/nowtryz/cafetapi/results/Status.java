package net.nowtryz.cafetapi.results;

public enum Status {
	OK("ok"),
	ERROR("error");
	
	public final String value;
	
	Status(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}
