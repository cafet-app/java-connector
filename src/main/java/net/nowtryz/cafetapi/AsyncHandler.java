package net.nowtryz.cafetapi;

import java.io.IOException;

import net.nowtryz.cafetapi.datas.CafetData;
import net.nowtryz.cafetapi.exceptions.APIException;
import net.nowtryz.cafetapi.results.ArrayResult;
import net.nowtryz.cafetapi.results.DataResult;
import net.nowtryz.cafetapi.results.LoginResult;
import net.nowtryz.cafetapi.results.Result;

public interface AsyncHandler {
	public void handleException(APIException e);
	public void handleException(IOException e);

	static public interface AsyncResultHandler<S extends Result> extends AsyncHandler {
		public void handleResult(S result) throws APIException, IOException;
	}

	static public interface AsyncSuccessHandler                    extends AsyncResultHandler<Result> {}
	static public interface AsyncLoginHandler                      extends AsyncResultHandler<LoginResult> {}
	static public interface AsyncDataHandler<T extends CafetData>  extends AsyncResultHandler<DataResult<T>> {}
	static public interface AsyncArrayHandler<T extends CafetData> extends AsyncResultHandler<ArrayResult<T>> {}	
}
