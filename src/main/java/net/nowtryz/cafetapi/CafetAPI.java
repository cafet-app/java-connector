package net.nowtryz.cafetapi;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.nowtryz.cafetapi.AsyncHandler.AsyncArrayHandler;
import net.nowtryz.cafetapi.AsyncHandler.AsyncDataHandler;
import net.nowtryz.cafetapi.AsyncHandler.AsyncLoginHandler;
import net.nowtryz.cafetapi.AsyncHandler.AsyncSuccessHandler;
import net.nowtryz.cafetapi.datas.Choice;
import net.nowtryz.cafetapi.datas.Client;
import net.nowtryz.cafetapi.datas.Expense;
import net.nowtryz.cafetapi.datas.ExpenseDetail;
import net.nowtryz.cafetapi.datas.Formula;
import net.nowtryz.cafetapi.datas.FormulaBought;
import net.nowtryz.cafetapi.datas.OrderBuilder.FormulaBuilder;
import net.nowtryz.cafetapi.datas.Product;
import net.nowtryz.cafetapi.datas.ProductBought;
import net.nowtryz.cafetapi.datas.ProductGroup;
import net.nowtryz.cafetapi.datas.Reload;
import net.nowtryz.cafetapi.exceptions.APIException;
import net.nowtryz.cafetapi.exceptions.HTTPException;
import net.nowtryz.cafetapi.exceptions.PermissionException;
import net.nowtryz.cafetapi.exceptions.QueryException;
import net.nowtryz.cafetapi.exceptions.ServerErrorException;
import net.nowtryz.cafetapi.http.HttpManager;
import net.nowtryz.cafetapi.http.HttpManager.HttpMethod;
import net.nowtryz.cafetapi.parsers.ArrayParser;
import net.nowtryz.cafetapi.parsers.DataParser;
import net.nowtryz.cafetapi.parsers.DefaultParser;
import net.nowtryz.cafetapi.parsers.LoginParser;
import net.nowtryz.cafetapi.results.ArrayResult;
import net.nowtryz.cafetapi.results.DataResult;
import net.nowtryz.cafetapi.results.LoginResult;
import net.nowtryz.cafetapi.results.Result;
import net.nowtryz.cafetapi.user.User;
import net.nowtryz.cafetapi.utils.Base64Coder;

public class CafetAPI {
	/*
	 * Constants
	 */
	private static final String SERVER_API_VERSION = "1.0.0-alpha";
	private static final String THREAD_NAME = "async-api-"/* n */;
	private static CafetAPI instance;
	private static int asyncThreads = 1;

	public static final String NAME, VERSION, BUILD, RELEASE_DATE;
	static {
		Properties properties = new Properties();
		try (InputStream is = CafetAPI.class.getResourceAsStream("api.properties");
				BufferedInputStream bis = new BufferedInputStream(is);) {
			properties.load(bis);
		} catch (Exception e) {}
		NAME = properties.getProperty("name");
		VERSION = properties.getProperty("version");
		BUILD = properties.getProperty("build");
		RELEASE_DATE = properties.getProperty("release.date");
	}

	/*
	 * Properties
	 */
	private HttpManager manager;
	private String serverVersion;
	private User user;

	/*
	 * Static methods
	 */

	public static String getVersion() {
		return SERVER_API_VERSION;
	}

	public static CafetAPI getInstance() {
		return instance;
	}

	public synchronized static void setInstance(CafetAPI instance) {
		CafetAPI.instance = instance;
	}

	/*
	 * Constuctor
	 */

	public CafetAPI(URL url) {
		this.manager = HttpManager.newInstance(url);
		CafetAPI.setInstance(this);
	}

	/*
	 * Getters
	 */

	/**
	 * Returns the serverVersion of the CafetAPI
	 * 
	 * @return the serverVersion
	 * @since API 1.0 (2018)
	 */
	public final String getServerVersion() {
		return serverVersion;
	}

	/**
	 * Returns the user of the CafetAPI
	 * 
	 * @return the user
	 * @since API 1.0 (2018)
	 */
	public final User getUser() {
		return user;
	}

	/**
	 * Returns the url of the server
	 * 
	 * @return the user
	 * @since API 1.0 (2018)
	 */
	public final URL getUrl() {
		return manager.getUrl();
	}

	/*
	 * Setters
	 */

	public final void setUrl(URL url) {
		manager.setUrl(url);
	}

	/*
	 * Private methods
	 */

	private final QueryException checkPermissionException(ServerErrorException e) {
		if (e.getResult().getErrorCode().equals("02-003"))
			return new PermissionException(e);
		else
			return e;
	}

	/*
	 * Basics
	 */

	public LoginResult login(String email, String password) throws IOException, APIException {
		HashMap<String, String> headers = new HashMap<String, String>();
		String endodedBasicAuth = Base64.getEncoder().encodeToString(new String(email + ":" + password).getBytes());
		LoginResult result;

		headers.put("Authorization", "Basic " + endodedBasicAuth);

		try {
			result = LoginParser.parseResponse(manager.query(HttpMethod.POST, "user/login", null, headers));
			manager.setSessionId(result.getSessionId());
			user = result.getUser();

			return result;
		} catch (HTTPException e) {
			throw ServerExceptionsManager.handle(e);
		}
	}

	public void asyncLogin(AsyncLoginHandler handler, String email, String password) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(login(email, password));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, "login-" + (asyncThreads++)).start();
	}

	/*
	 * Fetchers
	 */

	public ArrayResult<Client> getClients() throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/clients"), Client.class, Client[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetClients(AsyncArrayHandler<Client> handler) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getClients());
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<Client> getClient(int id) throws APIException, IOException {
		try {
			return DataParser.parseResponse(manager.query(HttpMethod.GET, "cafet/clients/" + id), Client.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetClient(AsyncDataHandler<Client> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getClient(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<Reload> getClientReloads(int id) throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/clients/" + id + "/reloads"), Reload.class,
					Reload[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetClientReloads(AsyncArrayHandler<Reload> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getClientReloads(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<Expense> getClientExpenses(int id) throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/clients/" + id + "/expenses"), Expense.class,
					Expense[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetClientExpenses(AsyncArrayHandler<Expense> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getClientExpenses(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<ExpenseDetail> getExpenseDetails(int id) throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/expenses/" + id + "/details"),
					ExpenseDetail.class, ExpenseDetail[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetExpenseDetails(AsyncArrayHandler<ExpenseDetail> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getExpenseDetails(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<ProductBought> getProductBought(int id) throws APIException, IOException {
		try {
			return DataParser.parseResponse(manager.query(HttpMethod.GET, "cafet/products_bought/" + id), ProductBought.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetProductBought(AsyncDataHandler<ProductBought> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getProductBought(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<FormulaBought> getFormulaBought(int id) throws APIException, IOException {
		try {
			return DataParser.parseResponse(manager.query(HttpMethod.GET, "cafet/formulas_bought/" + id), FormulaBought.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetFormulaBought(AsyncDataHandler<FormulaBought> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getFormulaBought(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<ProductGroup> getProductGroups() throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/groups"), ProductGroup.class,
					ProductGroup[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetProductGroups(AsyncArrayHandler<ProductGroup> handler) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getProductGroups());
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<ProductGroup> getProductGroup(int id) throws APIException, IOException {
		try {
			return DataParser.parseResponse(manager.query(HttpMethod.GET, "cafet/formulas_bought/" + id), ProductGroup.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetProductGroup(AsyncDataHandler<ProductGroup> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getProductGroup(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<Product> getGroupProducts(int id, boolean showUnviewable) throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/groups/" + id + "/products" + (showUnviewable ? "?hidden" : "")), Product.class, Product[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetGroupProducts(AsyncArrayHandler<Product> handler, int id, boolean showUnviewable) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getGroupProducts(id, showUnviewable));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<ProductBought> getFormumaBoughtProducts(int id) throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/formulas_bought/" + id + "/products"),
					ProductBought.class, ProductBought[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetFormumaBoughtProducts(AsyncArrayHandler<ProductBought> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getFormumaBoughtProducts(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<Product> getProduct(int id) throws APIException, IOException {
		try {
			return DataParser.parseResponse(manager.query(HttpMethod.GET, "cafet/products/" + id), Product.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetProduct(AsyncDataHandler<Product> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getProduct(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<Formula> getFormulas(boolean showUnviewable) throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(
					manager.query(HttpMethod.GET, "cafet/formulas" + (showUnviewable ? "?hidden" : "")), Formula.class,
					Formula[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetFormulas(AsyncArrayHandler<Formula> handler, boolean showUnviewable) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getFormulas(showUnviewable));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<Formula> getFormula(int id) throws APIException, IOException {
		try {
			return DataParser.parseResponse(manager.query(HttpMethod.GET, "cafet/formulas/" + id), Formula.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetFormula(AsyncDataHandler<Formula> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getFormula(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<Choice> getFormulaChoices(int id) throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/formulas/" + id + "/choices"), Choice.class,
					Choice[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetFormulaChoices(AsyncArrayHandler<Choice> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getFormulaChoices(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<Choice> getChoice(int id) throws APIException, IOException {
		try {
			return DataParser.parseResponse(manager.query(HttpMethod.GET, "cafet/choices/" + id), Choice.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncGetChoice(AsyncDataHandler<Choice> handler, int id) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(getChoice(id));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public ArrayResult<Client> searchClient(String expression) throws APIException, IOException {
		try {
			return ArrayParser.parseResponse(manager.query(HttpMethod.GET, "cafet/clients/search/" + expression), Client.class,
					Client[].class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSearchClient(AsyncArrayHandler<Client> handler, String expression) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(searchClient(expression));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	/*
	 * Adders
	 */

	public DataResult<ProductGroup> addProductGroup(String name) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("name", new JsonPrimitive(name));

		try {
			return DataParser.parseResponse(manager.query(HttpMethod.POST, "cafet/groups", body), ProductGroup.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncAddProductGroup(AsyncDataHandler<ProductGroup> handler, String name) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(addProductGroup(name));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<Product> addProduct(String name, int groupId) throws APIException, IOException {
		return addProduct(name, groupId, 0, true);
	}

	public DataResult<Product> addProduct(String name, int groupId, double price, boolean viewable)
			throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("group", new JsonPrimitive(groupId));
		body.add("name", new JsonPrimitive(name));
		body.add("price", new JsonPrimitive(price));
		body.add("viewable", new JsonPrimitive(viewable));

		try {
			return DataParser.parseResponse(manager.query(HttpMethod.POST, "cafet/products", body), Product.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncAddProduct(AsyncDataHandler<Product> handler, String name, int groupId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(addProduct(name, groupId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<Formula> addFormula(String name) throws APIException, IOException {
		return addFormula(name, 0, true);
	}

	public DataResult<Formula> addFormula(String name, double price, boolean viewable)
			throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("name", new JsonPrimitive(name));
		body.add("price", new JsonPrimitive(price));
		body.add("viewable", new JsonPrimitive(viewable));

		try {
			return DataParser.parseResponse(manager.query(HttpMethod.POST, "cafet/formulas", body), Formula.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncAddFormula(AsyncDataHandler<Formula> handler, String name) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(addFormula(name));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public DataResult<Choice> addChoice(String name, int formulaId) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("name", new JsonPrimitive(name));

		try {
			return DataParser.parseResponse(manager.query(HttpMethod.POST, "cafet/formulas/" + formulaId + "/choices", body),
					Choice.class);
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncAddChoice(AsyncDataHandler<Choice> handler, String name, int formulaId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(addChoice(name, formulaId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result addProductToChoice(int choiceId, int... productIds) throws APIException, IOException {
		JsonObject body = new JsonObject();
		JsonArray products = new JsonArray();
		body.add("add", products);
		for (int product : productIds) {
			products.add(product);
		}

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/choices/" + choiceId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncAddProductToChoice(AsyncSuccessHandler handler, int choiceId, int productId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(addProductToChoice(choiceId, productId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	/*
	 * Updaters
	 */

	public Result setProductGroupDisplayName(int groupId, String displayName) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("displayName", new JsonPrimitive(displayName));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/groups/" + groupId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetProductGroupDisplayName(AsyncSuccessHandler handler, int groupId, String displayName) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setProductGroupDisplayName(groupId, displayName));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setProductGroupName(int groupId, String name) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("name", new JsonPrimitive(name));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/groups/" + groupId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetProductGroupName(AsyncSuccessHandler handler, int groupId, String name) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setProductGroupName(groupId, name));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setProductName(int productId, String name) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("name", new JsonPrimitive(name));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/products/" + productId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetProductName(AsyncSuccessHandler handler, int productId, String name) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setProductName(productId, name));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setProductPrice(int productId, float price) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("price", new JsonPrimitive(price));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/products/" + productId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetProductPrice(AsyncSuccessHandler handler, int productId, float price) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					setProductPrice(productId, price);
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setProductGroup(int productId, int groupId) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("group", new JsonPrimitive(groupId));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/products/" + productId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetProductGroup(AsyncSuccessHandler handler, int productId, int groupId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setProductGroup(productId, groupId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setProductImage(int productId, Image image, String format) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("image", new JsonPrimitive(Base64Coder.encodeImage(image, format)));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/products/" + productId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetProductImage(AsyncSuccessHandler handler, int productId, Image image, String format) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setProductImage(productId, image, format));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setProductVisibility(int productId, boolean visilility) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("viewable", new JsonPrimitive(visilility));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/products/" + productId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetProductVisibility(AsyncSuccessHandler handler, int productId, boolean visilility) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setProductVisibility(productId, visilility));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setFormulaName(int formulaId, String name) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("name", new JsonPrimitive(name));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/formulas/" + formulaId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetFormulaName(AsyncSuccessHandler handler, int formulaId, String name) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setFormulaName(formulaId, name));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setFormulaPrice(int formulaId, float price) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("price", new JsonPrimitive(price));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/formulas/" + formulaId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetFormulaPrice(AsyncSuccessHandler handler, int formulaId, float price) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setFormulaPrice(formulaId, price));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setFormulaImage(int formulaId, Image image, String format) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("image", new JsonPrimitive(Base64Coder.encodeImage(image, format)));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/formulas/" + formulaId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetFormulaImage(AsyncSuccessHandler handler, int formulaId, Image image, String format) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setFormulaImage(formulaId, image, format));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setFormulaVisibility(int formulaId, boolean visilility) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("viewable", new JsonPrimitive(visilility));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/formulas/" + formulaId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetFormulaVisibility(AsyncSuccessHandler handler, int formulaId, boolean visilility) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setFormulaVisibility(formulaId, visilility));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result setChoiceName(int choiceId, String name) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("name", new JsonPrimitive(name));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/choices/" + choiceId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSetChoiceName(AsyncSuccessHandler handler, int choiceId, String name) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(setChoiceName(choiceId, name));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	/*
	 * Removers
	 */

	public Result removeProductFromChoice(int choiceId, int... productIds) throws APIException, IOException {
		JsonObject body = new JsonObject();
		JsonArray products = new JsonArray();
		body.add("remove", products);
		for (int product : productIds) {
			products.add(product);
		}

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.PATCH, "cafet/choices/" + choiceId, body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncRemoveProductFromChoice(AsyncSuccessHandler handler, int choiceId, int productId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(removeProductFromChoice(choiceId, productId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result removeProduct(int productId) throws APIException, IOException {
		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.DELETE, "cafet/products/" + productId));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncRemoveProduct(AsyncSuccessHandler handler, int productId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(removeProduct(productId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result removeProductGroup(int groupId) throws APIException, IOException {
		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.DELETE, "cafet/groups/" + groupId));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncRemoveProductGroup(AsyncSuccessHandler handler, int groupId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(removeProductGroup(groupId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result removeFormula(int formulaId) throws APIException, IOException {
		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.DELETE, "cafet/formulas/" + formulaId));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncRemoveFormula(AsyncSuccessHandler handler, int formulaId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(removeFormula(formulaId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result removeFormulaChoice(int choiceId) throws APIException, IOException {
		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.DELETE, "cafet/choices/" + choiceId));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncRemoveFormulaChoice(AsyncSuccessHandler handler, int choiceId) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(removeFormulaChoice(choiceId));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	/*
	 * Action savers
	 */

	public Result saveReload(int clientId, float amount) throws APIException, IOException {
		JsonObject body = new JsonObject();
		body.add("client_id", new JsonPrimitive(clientId));
		body.add("amount", new JsonPrimitive(amount));

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.POST, "cafet/reloads", body));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSaveReload(AsyncSuccessHandler handler, int clientId, float amount) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(saveReload(clientId, amount));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}

	public Result saveOrder(int clientId, Map<Product, Integer> products, Map<FormulaBuilder, Product[]> formulas)
			throws APIException, IOException {
		Ordered[] order = new Ordered[products.size() + formulas.size()];
		int i = 0;

		for (Product p : products.keySet()) {
			order[i] = new Ordered.ProductOrdered(p, products.get(p));
			i++;
		}

		for (FormulaBuilder b : formulas.keySet()) {
			order[i] = new Ordered.FormulaOrdered(b.getFormula(), 1, formulas.get(b));
			i++;
		}

		try {
			return DefaultParser.parseResponse(manager.query(HttpMethod.POST, "cafet/clients/" + clientId + "/order", new Gson().toJsonTree(order)));
		} catch (ServerErrorException e) {
			throw checkPermissionException(e);
		}
	}

	public void asyncSaveOrder(AsyncSuccessHandler handler, int clientId, Map<Product, Integer> products,
			Map<FormulaBuilder, Product[]> formulas) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					handler.handleResult(saveOrder(clientId, products, formulas));
				} catch (APIException e) {
					handler.handleException(e);
				} catch (IOException e) {
					handler.handleException(e);
				}
			}
		}, THREAD_NAME + (asyncThreads++)).start();
	}
}
