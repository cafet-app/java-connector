package net.nowtryz.cafetapi.exceptions;

import net.nowtryz.cafetapi.results.ErrorResult;

/**
 * @author Damien
 * @since API 1.0 (2018)
 */
public class PermissionException extends QueryException {
	private static final long serialVersionUID = -3318436575528824430L;
	private final ErrorResult result;
	

	public PermissionException(ErrorResult result) {
		super(result.getErrorMessage());
		this.result = result;
	}

	public PermissionException(ServerErrorException e) {
		super(e.getResult().getErrorMessage(), e);
		this.result = e.getResult();
	}

	/**
	 * Returns the result of the PermissionException
	 * @return the result
	 * @since API 1.0 (2018)
	 */
	public ErrorResult getResult() {
		return result;
	}

}
