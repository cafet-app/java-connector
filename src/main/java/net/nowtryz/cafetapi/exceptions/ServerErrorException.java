package net.nowtryz.cafetapi.exceptions;

import net.nowtryz.cafetapi.results.ErrorResult;

public class ServerErrorException extends ResultException {
	private static final long serialVersionUID = 8508658477190781475L;
	private final ErrorResult result;

	public ServerErrorException(ErrorResult result) {
		super(
			result.getErrorMessage()
			+ (result.hasAdditionalMessage() ? ": " + result.getAdditionalMessage() : "")
		);
		this.result = result;
	}

	/**
	 * Returns the result of the ServerErrorException
	 * @return the result
	 * @since API 1.0 (2018)
	 */
	public ErrorResult getResult() {
		return result;
	}

}
