package net.nowtryz.cafetapi.exceptions;

public class IllegalResultException extends ResultSemanticException {
	private static final long serialVersionUID = -2766290917809184084L;

	public IllegalResultException(String arg0) {
		super(arg0);
	}

	public IllegalResultException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
