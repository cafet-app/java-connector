package net.nowtryz.cafetapi.exceptions;

/**
 * @author Damien
 * @since API 1.0 (2018)
 */
public class AuthenticationException extends APIException {
	private static final long serialVersionUID = 1460555568556655048L;

	/**
	 * @param arg0
	 * @since API 1.0 (2018)
	 */
	public AuthenticationException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @since API 1.0 (2018)
	 */
	public AuthenticationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
