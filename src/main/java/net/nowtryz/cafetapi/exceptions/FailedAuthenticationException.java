/**
 * 
 */
package net.nowtryz.cafetapi.exceptions;

/**
 * @author Damien
 * @since API 1.0 (2018)
 */
public class FailedAuthenticationException extends AuthenticationException {
	private static final long serialVersionUID = -2753120313704039432L;

	/**
	 * @param arg0
	 * @since API 1.0 (2018)
	 */
	public FailedAuthenticationException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @since API 1.0 (2018)
	 */
	public FailedAuthenticationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
