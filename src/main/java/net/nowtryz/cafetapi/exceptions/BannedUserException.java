/**
 * 
 */
package net.nowtryz.cafetapi.exceptions;

/**
 * @author Damien
 * @since API 1.0 (2018)
 */
public class BannedUserException extends AuthenticationException {
	private static final long serialVersionUID = -7701677420454255237L;

	/**
	 * @param arg0
	 * @since API 1.0 (2018)
	 */
	public BannedUserException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @since API 1.0 (2018)
	 */
	public BannedUserException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
