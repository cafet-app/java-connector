/**
 * API exceptions
 * @author damie
 * @since API 1.0 (2018)
 */
package net.nowtryz.cafetapi.exceptions;