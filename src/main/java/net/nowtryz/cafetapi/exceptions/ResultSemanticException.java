package net.nowtryz.cafetapi.exceptions;

public abstract class ResultSemanticException extends ResultException {
	private static final long serialVersionUID = -4194260172593886825L;

	public ResultSemanticException(String arg0) {
		super(arg0);
	}

	public ResultSemanticException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ResultSemanticException(Throwable arg0) {
		super(arg0);
	}

}
