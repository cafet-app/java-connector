/**
 * 
 */
package net.nowtryz.cafetapi.exceptions;

import net.nowtryz.cafetapi.http.HTTPResponse;

/**
 * @author Damien
 * @since API 1.0 (2018)
 */
public class HTTPException extends QueryException {
	private static final long serialVersionUID = -3920714431790576493L;
	
	private int code;
	private HTTPResponse response;
	
	/**
	 */
	public HTTPException(String message, int code, HTTPResponse response) {
		super(message);
		this.code = code;
		this.response = response;
	}

	public int getCode() {
		return code;
	}

	public HTTPResponse getResponse() {
		return response;
	}

}
