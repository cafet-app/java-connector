package net.nowtryz.cafetapi.exceptions;

public abstract class QueryException extends APIException {
	private static final long serialVersionUID = 1343027426180919696L;

	public QueryException(String arg0) {
		super(arg0);
	}
	
	public QueryException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public QueryException(Throwable arg0) {
		super(arg0);
	}

}
