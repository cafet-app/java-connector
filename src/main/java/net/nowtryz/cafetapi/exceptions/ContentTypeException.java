package net.nowtryz.cafetapi.exceptions;

import net.nowtryz.cafetapi.http.HTTPResponse;

public class ContentTypeException extends QueryException {
	private static final long serialVersionUID = 8024379220197197046L;
	private HTTPResponse response;

	public ContentTypeException(String arg0, HTTPResponse response) {
		super(arg0);
		this.response = response;
	}

	public HTTPResponse getResponse() {
		return response;
	}
}
