package net.nowtryz.cafetapi.exceptions;

public class IllegalStatusException extends ResultSemanticException {
	private static final long serialVersionUID = -6447737193812940565L;

	public IllegalStatusException(String arg0) {
		super(arg0);
	}
}
