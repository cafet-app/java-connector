package net.nowtryz.cafetapi.exceptions;

public abstract class ResultException extends QueryException {
	private static final long serialVersionUID = 8446924584236165914L;


	public ResultException(String arg0) {
		super(arg0);
	}

	public ResultException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}


	public ResultException(Throwable arg0) {
		super(arg0);
	}
}
