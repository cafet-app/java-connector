/**
 * 
 */
package net.nowtryz.cafetapi.exceptions;

/**
 * @author Damien
 * @since API 1.0 (2018)
 */
public class APIException extends Exception {
	private static final long serialVersionUID = 7067177797976337130L;

	/**
	 * @param arg0
	 * @since API 1.0 (2018)
	 */
	public APIException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @since API 1.0 (2018)
	 */
	public APIException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public APIException(Throwable arg0) {
		super(arg0);
	}

}
