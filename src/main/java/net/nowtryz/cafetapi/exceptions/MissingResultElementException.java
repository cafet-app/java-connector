package net.nowtryz.cafetapi.exceptions;

public class MissingResultElementException extends ResultSemanticException {

	private static final long serialVersionUID = 5999633014577793206L;

	public MissingResultElementException(String arg0) {
		super(arg0);
	}

}
