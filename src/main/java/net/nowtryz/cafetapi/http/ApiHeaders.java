package net.nowtryz.cafetapi.http;

public final class ApiHeaders {

	private ApiHeaders() {
		// no instanciations
	}
	
	/**
	 * Session header to send to the server in order to identify the user
	 */
	public static final String SESSION = "Session";
	
	/**
	 * Header provided by the server to now the computation duration
	 */
	public static final String RUNTIME = "Runtime";

}
