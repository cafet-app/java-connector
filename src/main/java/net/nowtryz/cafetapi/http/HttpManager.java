package net.nowtryz.cafetapi.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.JsonElement;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.ContentTypeException;
import net.nowtryz.cafetapi.exceptions.HTTPException;
import net.nowtryz.cafetapi.exceptions.QueryException;

public class HttpManager {
	private static final String USER_AGENT = "CafetAPI/" + CafetAPI.VERSION;
	private static final String PATH_PREFIX = "api/v1/";
	private static final String ENCODING = "UTF-8";
	/**
	 * Request body mime type and accepted mime type
	 */
	private static final String MIME_TYPE = "application/json";
	/**
	 * Desired mime type from the server
	 */
	private static final String SERVER_MIME_TYPE = MIME_TYPE + "; charset=" + ENCODING;
	private static HttpManager instance;

	private URL url;
	private String sessionId;


	public static HttpManager newInstance(URL url) {
		instance = new HttpManager(url);
		return instance;
	}

	public static HttpManager getInstance() {
		return instance;
	}

	private HttpManager(URL url) {
		this.url = url;
	}

	public final String getSessionId() {
		return sessionId;
	}


	public final void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the url
	 */
	public final URL getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public final void setUrl(URL url) {
		this.url = url;
	}
	
	/**
	 * Perform an http rest query to the server with a body
	 * @param path a relative path to the api root path ((server)/api/v1/...)
	 * @param body a body formated in json to be sent to the server
	 * @return {@link HTTPResponse} the reponse from the server
	 * @throws IOException if path is null or if an other I/O exception occurs.
	 * @throws URISyntaxException 
	 */
	public HTTPResponse query(HttpMethod method, String path, JsonElement body) throws IOException, QueryException {
		return query(method, path, body, null);
	}
	
	/**
	 * Perform an http rest query to the server
	 * @param path a relative path to the api root path ((server)/api/v1/...)
	 * @return {@link HTTPResponse} the reponse from the server
	 * @throws IOException if path is null or if an other I/O exception occurs.
	 * @throws URISyntaxException 
	 */
	public HTTPResponse query(HttpMethod method, String path) throws IOException, QueryException {
		return query(method, path, null, null);
	}

	/**
	 * Perform an http rest query to the server with a body and additionnal headers
	 * @param path a relative path to the api root path ((server)/api/v1/...)
	 * @param body a body formated in json to be sent to the server
	 * @param headers additional headers
	 * @return {@link HTTPResponse} the reponse from the server
	 * @throws IOException if path is null or if an other I/O exception occurs.
	 * @throws QueryException various query exceptions depending on the response from the server
	 * @throws URISyntaxException 
	 */
	public HTTPResponse query(HttpMethod method, String path, JsonElement body, Map<String, String> headers) throws IOException, QueryException {
		URL resourceUrl = new URL(this.url, PATH_PREFIX + path);
		StringBuilder responseBuilder = new StringBuilder();
		String responseBody;
		CloseableHttpClient httpclient = HttpClients.createSystem();
		HttpRequestBase httpRequest;
		CloseableHttpResponse httpResponse;
		HTTPResponse response;
		
		switch (method) {
		case DELETE:
			httpRequest = new HttpDelete(resourceUrl.toString());
			break;
		case GET:
			httpRequest = new HttpGet(resourceUrl.toString());
			break;
		case HEAD:
			httpRequest = new HttpHead(resourceUrl.toString());
			break;
		case PATCH:
			httpRequest = fillEntity(new HttpPatch(resourceUrl.toString()), body);
			break;
		case POST:
			httpRequest = fillEntity(new HttpPost(resourceUrl.toString()), body);
			break;
		case PUT:
			httpRequest = fillEntity(new HttpPut(resourceUrl.toString()), body);
			break;
		default:
			httpRequest = new HttpGet();
		}
		
		httpRequest.addHeader(HttpHeaders.USER_AGENT, USER_AGENT);
		httpRequest.addHeader(HttpHeaders.ACCEPT, MIME_TYPE);
		httpRequest.addHeader(HttpHeaders.CONTENT_TYPE, MIME_TYPE);
		if (sessionId != null) httpRequest.addHeader(ApiHeaders.SESSION, this.sessionId);
		if (headers != null) headers.forEach((name, value)-> httpRequest.addHeader(name, value));
		
		httpResponse = httpclient.execute(httpRequest);
		
		HttpEntity entity = httpResponse.getEntity();
		if (entity != null) {
		    entity = new BufferedHttpEntity(entity);
		    
		    
		    try(
				InputStreamReader isr = new InputStreamReader(entity.getContent(), ENCODING);
				BufferedReader in = new BufferedReader(isr)
			) {
				for (int c; (c = in.read()) >= 0;)
					responseBuilder.append((char)c);
				responseBody = responseBuilder.toString();
				
				response = new HTTPResponse(
			    		responseBody,
			    		entity.getContentType().getValue(),
			    		httpResponse.getStatusLine().getStatusCode(),
			    		httpResponse.getStatusLine().getReasonPhrase(),
			    		entity.getContentLength(),
			    		httpResponse.getAllHeaders()
	    		);
				
				if(entity.getContentLength() > 0 && !SERVER_MIME_TYPE.equalsIgnoreCase(entity.getContentType().getValue())) {
					throw new ContentTypeException("Got " + entity.getContentType().getValue() + " while expecting " + SERVER_MIME_TYPE, response);
				}
			} finally {
				entity.getContent().close();
				httpResponse.close();
				httpclient.close();
			}
		} else {
			response = new HTTPResponse(
		    		null,
		    		null,
		    		httpResponse.getStatusLine().getStatusCode(),
		    		httpResponse.getStatusLine().getReasonPhrase(),
		    		0L,
		    		httpResponse.getAllHeaders()
    		);
		}
		
		if(response.getCode()/100 > 3) {
			throw new HTTPException("The server responded whith an " + response.getCode() + " " + response.getMessage() + " error code", response.getCode(), response);
		}

		return response;
	}
	
	private HttpEntityEnclosingRequestBase fillEntity(HttpEntityEnclosingRequestBase request,  JsonElement body) {
		if(body != null) {
			request.setEntity(new StringEntity(body.toString(), ContentType.APPLICATION_JSON));
		}
		return request;
	}

	/**
	 * This enumeration represents the HTTP methods supported by the server
	 * @author Damien
	 *
	 */
	public static enum HttpMethod {
		/**
		 * HTTP DELETE method.
		 */
		DELETE,
		/**
		 * HTTP GET method.
		 */
		GET,
		/**
		 * HTTP HEAD method.
		 */
		HEAD,
		/**
		 * HTTP POST method.
		 */
		POST,
		/**
		 * HTTP PUT method.
		 */
		PUT,
		/**
		 * HTTP PATCH method.
		 */
		PATCH
	}
}
