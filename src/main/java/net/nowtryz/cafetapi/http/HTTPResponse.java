package net.nowtryz.cafetapi.http;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

public class HTTPResponse {
	private String response, contentEncoding, message;
	private int code;
	private long contentLength;
	private Header[] headerFields;
	
	HTTPResponse(String response, String contentEncoding, int code, String message,long l, Header[] headers) {
		this.response = response;
		this.contentEncoding = contentEncoding;
		this.code = code;
		this.message = message;
		this.contentLength = l;
		this.headerFields = headers;
	}

	/**
	 * Returns the response of the HttpManager.HTTPResponse
	 * @return the response
	 * @since API 1.0 (2018)
	 */
	public final String getResponse() {
		return response;
	}

	/**
	 * Returns the contentEncoding of the HttpManager.HTTPResponse
	 * @return the contentEncoding
	 * @since API 1.0 (2018)
	 */
	public final String getContentEncoding() {
		return contentEncoding;
	}

	/**
	 * Returns the code of the HttpManager.HTTPResponse
	 * @return the code
	 * @since API 1.0 (2018)
	 */
	public final int getCode() {
		return code;
	}

	/**
	 * Returns the http message of the HttpManager.HTTPResponse
	 * @return the message
	 * @since API 0.4 (2018)
	 */
	public final String getMessage() {
		return message;
	}

	/**
	 * Returns the contentLength of the HttpManager.HTTPResponse
	 * @return the contentLength
	 * @since API 1.0 (2018)
	 */
	public final long getContentLength() {
		return contentLength;
	}

	/**
	 * Returns the headerFields of the HttpManager.HTTPResponse
	 * @return the headerFields
	 * @since API 1.0 (2018)
	 */
	public final Header[] getHeaderFields() {
		return headerFields;
	}

	/**
	 * Returns the headerFields of the HttpManager.HTTPResponse
	 * @return the headerFields
	 * @since API 1.0 (2018)
	 */
	public final Map<String, String> getHeaderFieldsMap() {
		Map<String,String> map = new HashMap<String, String>();
		for(Header h : headerFields) map.put(h.getName(), h.getValue());
		return map;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HTTPResponse [\nresponse=" + response + ",\ncontentEncoding=" + contentEncoding + ",\nmessage=" + message
				+ ",\ncode=" + code + ",\ncontentLength=" + contentLength + ",\nheaderFields=" + headerFields + "\n]";
	}
}