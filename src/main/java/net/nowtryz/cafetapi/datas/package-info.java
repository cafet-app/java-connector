/**
 * Data managing classes package
 * @author Damien
 * @since API 1.0 (2018)
 */
package net.nowtryz.cafetapi.datas;