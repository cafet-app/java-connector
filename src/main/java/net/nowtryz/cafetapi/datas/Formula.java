package net.nowtryz.cafetapi.datas;

import java.awt.Image;
import java.io.IOException;
import java.util.Calendar;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.APIException;

/**
 * @author Damien
 * @since 1.0
 */
public class Formula extends Payable {

	/**
	 * 
	 * @since 1.0
	 */
	public Formula() {
		super();
	}

	/**
	 * @param id
	 * @param name
	 * @param price
	 * @param viewable
	 * @param edit
	 * @since 1.0
	 */
	public Formula(int id, String name, Image image, float price, boolean viewable, Calendar edit) {
		super(id, name, image, price, viewable, edit);
	}

	/**
	 * Return the choices
	 * @return the choices
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public Choice[] getChoices() throws APIException, IOException {
		return CafetAPI.getInstance().getFormulaChoices(id).getDatas();
	}
	
	/**
	 * 
	 * @param name
	 * @return the choice created
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public Choice createNewChoice(String name) throws APIException, IOException {
		return CafetAPI.getInstance().addChoice(name, id).getData();
	}

	/* (non-Javadoc)
	 * @see com.essaim.cafet.datas.Payable#setName()
	 */
	@Override
	public final void setName(String name) throws APIException, IOException {
		CafetAPI.getInstance().setFormulaName(id, name);
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.essaim.cafet.datas.Payable#setPrice()
	 */
	@Override
	public final void setPrice(float price) throws APIException, IOException {
		CafetAPI.getInstance().setFormulaPrice(id, price);
		this.price = price;
	}

	/* (non-Javadoc)
	 * @see com.essaim.cafet.datas.Payable#setViewable()
	 */
	@Override
	public final void setViewable(boolean viewable) throws APIException, IOException {
		CafetAPI.getInstance().setFormulaVisibility(id, viewable);
		this.viewable = viewable;
	}

	/* (non-Javadoc)
	 * @see com.essaim.cafet.datas.Payable#delete()
	 */
	@Override
	public void delete() throws APIException, IOException {
		CafetAPI.getInstance().removeFormula(id);
		this.id = 0;
	}
	
	/* (non-Javadoc)
	 * @see com.essaim.cafet.datas.Payable#setImage()
	 */
	@Override
	public void setImage(Image image, String format) throws APIException, IOException {
		CafetAPI.getInstance().setFormulaImage(id, image, format);
		this.image = image;
	}
}
