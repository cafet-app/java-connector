package net.nowtryz.cafetapi.datas;

import java.awt.Image;
import java.io.IOException;
import java.util.Calendar;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.APIException;

/**
 * @author Damien
 * @since 1.0
 */
public class Product extends Payable {
	private int group;
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * @since API 1.0 (2018)
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + group;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @since API 1.0 (2018)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (group != other.group)
			return false;
		return true;
	}
	/**
	 * @param id
	 * @param name
	 * @param price
	 * @param viewable
	 * @param edit
	 * @since 1.0
	 */
	public Product(int id, String name, float price, int group, Image image, boolean viewable, Calendar edit) {
		super(id, name, image, price, viewable, edit);
		this.group = group;
	}
	/**
	 * @since 1.0
	 */
	public Product() {
		super();
	}
	/**
	 * Return the group
	 * @return the group
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final ProductGroup getGroup() throws APIException, IOException {
		return CafetAPI.getInstance().getProductGroup(group).getData();
	}
	/**
	 * Sets group
	 * @param group the group to set
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final void setGroup(ProductGroup group) throws APIException, IOException {
		CafetAPI.getInstance().setProductGroup(id, group.getId());
		this.group = group.getId();
	}
	/**
	 * Sets image
	 * @param image the image to set
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final void setImage(Image image, String format) throws IOException, APIException {
		CafetAPI.getInstance().setProductImage(id, image, format);
		this.image = image;
	}
	/**
	 * Sets name
	 * @param name the name to set
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	@Override
	public final void setName(String name) throws APIException, IOException {
		CafetAPI.getInstance().setProductName(id, name);
		this.name = name;
	}
	/**
	 * Sets price
	 * @param price the price to set
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	@Override
	public final void setPrice(float price) throws APIException, IOException {
		CafetAPI.getInstance().setProductPrice(id, price);
		this.price = price;
	}
	/**
	 * Sets viewable
	 * @param viewable the viewable to set
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	@Override
	public final void setViewable(boolean viewable) throws APIException, IOException {
		CafetAPI.getInstance().setProductVisibility(id, viewable);
		this.viewable = viewable;
	}
	/**
	 * Delete this product from the database
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final void delete() throws APIException, IOException {
		CafetAPI.getInstance().removeProduct(id);
		this.id = 0;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name;
	}
}
