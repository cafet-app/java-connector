package net.nowtryz.cafetapi.datas;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.APIException;

/**
 * @author Damien
 * @since 1.0
 */
public class FormulaBought extends ExpenseDetail {
	private int formula;
	private int id;

	/**
	 * @param id
	 * @param formula
	 * @param name
	 * @param client
	 * @param price
	 * @param quantity
	 * @param date
	 * @since 1.0
	 */
	public FormulaBought(int id, int formula, String name, int client, float price, int quantity, Calendar date) {
		super(name, client, price, quantity, date);
		this.formula = formula;
	}

	/**
	 * 
	 * @since 1.0
	 */
	public FormulaBought() {
		super();
		this.formula = 0;
	}

	/**
	 * Return the id
	 * @return the id
	 * @since 1.0
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Return the formula
	 * @return the formula
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final Formula getFormula() throws APIException, IOException {
		return CafetAPI.getInstance().getFormula(formula).getData();
	}

	/**
	 * Return the products
	 * @return the products
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final ProductBought[] getProducts() throws SQLException, APIException, IOException {
		return CafetAPI.getInstance().getFormumaBoughtProducts(id).getDatas();
	}
}
