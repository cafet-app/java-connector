package net.nowtryz.cafetapi.datas;

import java.io.IOException;
import java.util.Calendar;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.APIException;

/**
 * @author Damien
 * @since 1.0
 */
public class ProductBought extends ExpenseDetail {
	private int product;

	/**
	 * @param product
	 * @param name
	 * @param client
	 * @param price
	 * @param quantity
	 * @param date
	 * @since 1.0
	 */
	public ProductBought(int product, String name, int client, float price, int quantity, Calendar date) {
		super(name, client, price, quantity, date);
		this.product = product;
	}

	/**
	 * 
	 * @since 1.0
	 */
	public ProductBought() {
		super();
		this.product = 0;
	}

	/**
	 * Return the product
	 * @return the product
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final Product getProduct() throws APIException, IOException {
		return CafetAPI.getInstance().getProduct(product).getData();
	}
}
