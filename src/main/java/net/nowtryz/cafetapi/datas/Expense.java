package net.nowtryz.cafetapi.datas;

import java.io.IOException;
import java.util.Calendar;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.APIException;


/**
 * The Expense object is the one that stores every expense's information for
 * later use, it's generaly generated by the Client's method
 * {@link Client#getExpenses() getExpenses} or even directly whith the Data
 * static method {@link CafetAPI#getClientExpenses(int) getClientExpenses()}.
 * 
 * @author Damien (aka the guys who uses Eclipse generators)
 * @since 1.0
 */
public class Expense extends CafetData {
	private int id;
	private int client;
	private Calendar date;
	private float total;
	private float balanceAfterTransaction;

	/**
	 * The Expense object is the one that stores every expense's information for
	 * later use, it's generaly generated by the Client's method
	 * {@link Client#getExpenses() getExpenses} or even directly whith the Data
	 * static method {@link CafetAPI#getClientExpenses(int) getClientExpenses()}.
	 * 
	 * @param id - the id of the expense in the database
	 * @param client - the clien wich performed this transaction
	 * @param date - the date at wich the transaction has been completed (a {@link java.util.Calendar Calendar} object)
	 * @param total - the total price dued from the client
	 * @param balanceAfterTransaction - the balance of the clien after the transaction
	 * @since 1.0
	 * @see Expense
	 */
	public Expense(int id, int client, Calendar date, float total, float balanceAfterTransaction) {
		this.id = id;
		this.client = client;
		this.date = date;
		this.total = total;
		this.balanceAfterTransaction = balanceAfterTransaction;
	}

	/**
	 * The Expense object is the one that stores every expense's information for
	 * later use, it's generaly generated by the Client's method
	 * {@link Client#getExpenses() getExpenses} or even directly whith the Data
	 * static method {@link CafetAPI#getClientExpenses(int) getClientExpenses()}.
	 * 
	 * @since 1.0
	 * @see Expense
	 */
	public Expense() {
		this.id = 0;
		this.client = 0;
		this.date = Calendar.getInstance();
		this.total = 0.0f;
		this.balanceAfterTransaction = 0.0f;
	}

	/**
	 * Return the id of the expense
	 * 
	 * @return the id
	 * @since 1.0
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Return the client who has spent money
	 * 
	 * @return the client
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final Client getClient() throws APIException, IOException {
		return CafetAPI.getInstance().getClient(client).getData();
	}

	/**
	 * Return the date of the transaction in a {@link java.util.Calendar Calendar} object
	 * 
	 * @return the date
	 * @since 1.0
	 */
	public final Calendar getDate() {
		return date;
	}

	/**
	 * Return the total price of products the client bought
	 * 
	 * @return the total
	 * @since 1.0
	 */
	public final double getTotal() {
		return total;
	}

	/**
	 * Return the balance after the transaction
	 * 
	 * @return the balanceAfterTransaction
	 * @since 1.0
	 */
	public final float getBalanceAfterTransaction() {
		return balanceAfterTransaction;
	}
	
	/**
	 * Return the itemized bill
	 * 
	 * @return the balanceAfterTransaction
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final ExpenseDetail[] getDetails() throws APIException, IOException {
		return CafetAPI.getInstance().getExpenseDetails(id).getDatas();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Expense [id=" + id + ", client=" + client + ", date=" + date + ", total=" + total
				+ ", balanceAfterTransaction=" + balanceAfterTransaction + "]";
	}
}
