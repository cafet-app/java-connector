package net.nowtryz.cafetapi.datas;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Calendar;

import net.nowtryz.cafetapi.exceptions.APIException;
import net.nowtryz.cafetapi.utils.Lang;

/**
 * @author Damien
 * @since 1.0
 */
public abstract class Payable extends CafetData {
	protected int id;
	protected String name;
	protected Image image;
	protected float price;
	protected boolean viewable;
	protected Calendar edit;
	/**
	 * @param id
	 * @param name
	 * @param price
	 * @param viewable
	 * @param edit - the last edit (a {@link java.util.Calendar Calendar} object)
	 * @since 1.0
	 */
	Payable(int id, String name, Image image, float price, boolean viewable, Calendar edit) {
		this.id = id;
		this.image = image;
		this.name = name;
		this.price = price;
		this.viewable = viewable;
		this.edit = edit;
	}
	/**
	 * 
	 * @since 1.0
	 */
	Payable() {
		this.id = 0;
		this.image = new BufferedImage(20, 20, BufferedImage.TRANSLUCENT);
		this.name = Lang.get("Unknown");
		this.price = 0;
		this.viewable = false;
		this.edit = Calendar.getInstance();
	}
	/**
	 * Return the id
	 * @return the id
	 * @since 1.0
	 */
	public final int getId() {
		return id;
	}
	/**
	 * Return the name
	 * @return the name
	 * @since 1.0
	 */
	public final String getName() {
		return name;
	}
	/**
	 * Return the image
	 * @return the image
	 * @since 1.0
	 */
	public final Image getImage() {
		return image;
	}
	/**
	 * Return the price
	 * @return the price
	 * @since 1.0
	 */
	public final float getPrice() {
		return price;
	}
	/**
	 * Return the viewable
	 * @return the viewable
	 * @since 1.0
	 */
	public final boolean isViewable() {
		return viewable;
	}
	/**
	 * Return the edit date in a {@link java.util.Calendar Calendar} object
	 * @return the edit time
	 * @since 1.0
	 */
	public final Calendar getEdit() {
		return edit;
	}
	
	/**
	 * Id cannot be set!
	 * @since 1.0
	 */
	public final void setId() {}

	/**
	 * Sets image
	 * @param image the image to set
	 * @throws IOException 
	 * @since 1.0
	 */
	public abstract void setImage(Image image, String format) throws APIException, IOException;
	/**
	 * Sets name
	 * @param name the name to set
	 * @since 1.0
	 */
	public abstract void setName(String name) throws APIException, IOException;
	/**
	 * Sets price
	 * @param price the price to set
	 * @since 1.0
	 */
	public abstract void setPrice(float price) throws APIException, IOException;
	/**
	 * Sets viewable
	 * @param viewable the viewable to set
	 * @since 1.0
	 */
	public abstract void setViewable(boolean viewable) throws APIException, IOException;
	/***
	 * 
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public abstract void delete() throws APIException, IOException;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * @since API 1.0 (2018)
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((edit == null) ? 0 : edit.hashCode());
		result = prime * result + id;
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + Float.floatToIntBits(price);
		result = prime * result + (viewable ? 1231 : 1237);
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @since API 1.0 (2018)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payable other = (Payable) obj;
		if (edit == null) {
			if (other.edit != null)
				return false;
		} else if (!edit.equals(other.edit))
			return false;
		if (id != other.id)
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		}
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Float.floatToIntBits(price) != Float.floatToIntBits(other.price))
			return false;
		if (viewable != other.viewable)
			return false;
		return true;
	}
}
