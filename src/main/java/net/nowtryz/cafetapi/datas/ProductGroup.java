package net.nowtryz.cafetapi.datas;

import java.io.IOException;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.APIException;

/**
 * @author Damien
 * @since 1.0
 */
public class ProductGroup extends CafetData {
	private int id;
	private String name;
	private String displayName;

	/**
	 * @param id
	 * @param name
	 * @param displayName
	 * @since 1.0
	 */
	public ProductGroup(int id, String name, String displayName) {
		this.id = id;
		this.name = name;
		this.displayName = displayName;
	}

	/**
	 * Return the id
	 * @return the id
	 * @since 1.0
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Return the name
	 * @return the name
	 * @since 1.0
	 */
	public final String getName() {
		return name;
	}

	/**
	 * Return the displayName
	 * @return the displayName
	 * @since 1.0
	 */
	public final String getDisplayName() {
		return displayName;
	}
	
	/**
	 * Return products of this group exept unviewable ones
	 * @return Product[] if exists
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final Product[] getProducts() throws APIException, IOException {
		return CafetAPI.getInstance().getGroupProducts(id, false).getDatas();
	}
	
	/**
	 * Return products of this group included unviewable ones
	 * @return Product[] if exists
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final Product[] getAllProducts() throws APIException, IOException {
		return CafetAPI.getInstance().getGroupProducts(id, true).getDatas();
	}

	/**
	 * ID cannot be reset!
	 * @since 1.0
	 */
	public final void setId() {
	}

	/**
	 * Sets name
	 * @param name the name to set
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final void setName(String name) throws APIException, IOException {
		CafetAPI.getInstance().setProductGroupName(id, name);
		this.name = name;
	}

	/**
	 * Sets displayName
	 * @param displayName the displayName to set
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final void setDisplayName(String displayName) throws APIException, IOException {
		CafetAPI.getInstance().setProductGroupDisplayName(id, displayName);
		this.displayName = displayName;
	}
	
	/**
	 * Delete this group from the database
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public final void delete() throws APIException, IOException {
		CafetAPI.getInstance().removeProductGroup(id);
		this.id = 0;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.name;
	}
}
