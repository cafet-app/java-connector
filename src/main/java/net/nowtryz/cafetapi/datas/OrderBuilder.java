package net.nowtryz.cafetapi.datas;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.APIException;

/**
 * @author Damien
 * @since 1.0
 */
public class OrderBuilder {
	private HashMap<Product, Integer> orderProducts = new HashMap<Product, Integer>();
	private HashMap<FormulaBuilder, Product[]> orderFormula = new HashMap<FormulaBuilder, Product[]>();

	public OrderBuilder() {
		
	}
	
	public int orderProduct(Product product) {
		int count;
		
		if(!orderProducts.containsKey(product)) orderProducts.put(product, 0);
		
		count = orderProducts.get(product);
		orderProducts.put(product, ++count);
		
		return count;
	}
	
	public int removeProduct(Product product) {
		return orderProducts.remove(product);
	}
	
	public FormulaBuilder buildFormula(Formula formula) throws APIException, IOException {
		return new FormulaBuilder(formula);
	}
	
	public void save(CafetAPI api, int clientId) throws APIException, IOException {
		api.saveOrder(clientId, orderProducts, orderFormula);
	}
	
	public class FormulaBuilder {
		private Formula formula;
		private Choice[] choices;
		private HashMap<Choice, Product> products = new HashMap<Choice, Product>();

		/**
		 * 
		 * @throws IOException 
		 * @throws APIException 
		 * @since 1.0
		 */
		private FormulaBuilder(Formula formula) throws APIException, IOException {
			this.formula = formula;
			this.choices = formula.getChoices();
		}
		
		public Formula getFormula() {
			return this.formula;
		}
		
		public Choice[] getChoices() {
			return this.choices;
		}

		public FormulaBuilder setChoice(Choice choice, Product product) {
			if(!Arrays.asList(choices).contains(choice)) throw new IllegalArgumentException("this builder does not contain the given choice");
			
			if(products.containsKey(choice)) {
				products.replace(choice, products.get(choice), product);
			} else {
				products.put(choice, product);
			}
			
			return this;
		}
		
		public Product[] getOrder() {
			return this.products.values().toArray(new Product[products.size()]);
		}
		
		public void build() {
			orderFormula.put(this, getOrder());
		}
	}
}
