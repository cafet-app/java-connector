package net.nowtryz.cafetapi.datas;

import java.io.IOException;
import java.util.Arrays;

import net.nowtryz.cafetapi.CafetAPI;
import net.nowtryz.cafetapi.exceptions.APIException;
import net.nowtryz.cafetapi.utils.Lang;

/**
 * @author Damien
 * @since 1.0
 */
public class Choice extends CafetData {
	private int id;
	private String name;
	private Product[] choice;
	private int formula;

	/**
	 * 
	 * @since 1.0
	 */
	public Choice() {
		this.id = 0;
		this.name = Lang.get("Unknown");
		this.choice = new Product[0];
		this.formula = 0;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param formulaId
	 * @param choice
	 * @since API 1.0 (2018)
	 */
	public Choice(int id, String name, int formulaId, Product[] choice) {
		this.id = id;
		this.name = name;
		this.choice = choice;
		this.formula = formulaId;
	}

	/**
	 * Return the id
	 * @return the id
	 * @since 1.0
	 */
	public int getId() {
		return id;
	}

	/**
	 * Return the name
	 * @return the name
	 * @since 1.0
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the formula
	 * @return the formula
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public Formula getFormula() throws APIException, IOException {
		return CafetAPI.getInstance().getFormula(formula).getData();
	}

	/**
	 * Return the choice
	 * @return products of this choice
	 * @since 1.0
	 */
	public Product[] getProducts() {
		return choice;
	}
	
	/**
	 * 
	 * @param product
	 * @return boolean - true on success
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public void addProduct(Product product) throws APIException, IOException {
		CafetAPI.getInstance().addProductToChoice(id, product.getId());
	}
	
	/**
	 * 
	 * @param product
	 * @return boolean - true on success
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public void remove(Product product) throws APIException, IOException {
		CafetAPI.getInstance().removeProductFromChoice(id, product.getId());
	}

	/**
	 * Sets name
	 * @param name the name to set
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public void setName(String name) throws APIException, IOException {
		CafetAPI.getInstance().setChoiceName(this.id, name);
		this.name = name;
	}

	/**
	 * 
	 * 
	 * @throws IOException 
	 * @throws APIException 
	 * @since 1.0
	 */
	public void delete() throws APIException, IOException {
		CafetAPI.getInstance().removeFormulaChoice(id);
		this.id = 0;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 * @since API 1.0 (2018)
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(choice);
		result = prime * result + formula;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @since API 1.0 (2018)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Choice other = (Choice) obj;
		if (!Arrays.equals(choice, other.choice))
			return false;
		if (formula != other.formula)
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
