/**
 * 
 */
/**
 * Static objects package used to manipulate data
 * @author Damien
 * @since API 1.0 (2018)
 * @since 1.0
 */
package net.nowtryz.cafetapi.utils;