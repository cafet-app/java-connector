package net.nowtryz.cafetapi.utils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;

/**
 * @author Damien
 * @since 1.0
 */
public class Base64Coder {
	public static Image decodeImage(String image) {
		if(image == null || image.isEmpty()) return null;
		try {
			byte[] bytes = Base64.getDecoder().decode(image);
			return ImageIO.read(new ByteArrayInputStream(bytes));
		} catch (IOException | IllegalArgumentException e) {
			e.printStackTrace();
			return new BufferedImage(20, 20, BufferedImage.TRANSLUCENT);
		}
	}
	
	/**
	 * Convert an image to its base64 equivalent
	 * @param image the image to encode
	 * @param format a String containing the informal name of the format
	 * @return a string that represents the imgage encoded in Base 64
	 * @since 1.0
	 */
	public static String encodeImage(Image image, String format) {
		if(image == null || format == null || format.isEmpty()) return "";
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			ImageIO.write((RenderedImage) image, format, baos);
			byte[] bytes = baos.toByteArray();
			return Base64.getEncoder().encodeToString(bytes);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
