package net.nowtryz.cafetapi.utils;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import net.nowtryz.cafetapi.CafetAPI;

public class Lang {//FIXME change lang functionment
	private static final String BUNDLE_NAME = "fr_FR";
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(CafetAPI.class.getPackage().getName() + ".lang." + BUNDLE_NAME);

	private Lang() {
	}

	public static String get(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key.replace(' ', '_'));
		} catch (MissingResourceException e) {
			System.out.println("\"" + key + "\" hasn't translation in the lang file");
			return key;
		}
	}
}
