package net.nowtryz.cafetapi.utils;

/**
 * @author Damien
 * @since 1.0
 */
public class WordUtils {
	public static String capitalizeFirstLetter(String source) {
		if(source == null) return null;
		if(source.length() <= 1) return source.toUpperCase();
		return source.substring(0, 1).toUpperCase() + source.toLowerCase().substring(1);
	}
}