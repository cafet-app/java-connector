package net.nowtryz.cafetapi.parsers;

import java.awt.Image;
import java.util.Calendar;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import net.nowtryz.cafetapi.datas.ExpenseDetail;
import net.nowtryz.cafetapi.deserializers.CalendarDeserializer;
import net.nowtryz.cafetapi.deserializers.ExpenseDetailDeserializer;
import net.nowtryz.cafetapi.deserializers.ImageDeserializer;
import net.nowtryz.cafetapi.exceptions.IllegalResultException;
import net.nowtryz.cafetapi.exceptions.ResultException;
import net.nowtryz.cafetapi.http.ApiHeaders;
import net.nowtryz.cafetapi.http.HTTPResponse;
import net.nowtryz.cafetapi.results.Result;

public abstract class AbstractParser<T extends Result> {
	protected static final String TYPE = "type";
	protected static final Gson CLASS_DECODER = new GsonBuilder()
			.registerTypeAdapter(Image.class, new ImageDeserializer())
			.registerTypeAdapter(Calendar.class, new CalendarDeserializer())
			.registerTypeAdapter(ExpenseDetail.class, new ExpenseDetailDeserializer())
			.create();
	

	protected float getComputing(HTTPResponse response) {
		return Float.parseFloat(response.getHeaderFieldsMap().get(ApiHeaders.RUNTIME));
	}

	protected JsonElement parseJson(String resultString) throws IllegalResultException {
		try {
			return new JsonParser().parse(resultString);
		} catch (JsonSyntaxException e) {
			throw new IllegalResultException("unable to parse: " + resultString, e);
		}
	}
	
	protected Gson getClassDecoder() {
		return CLASS_DECODER;
	}
	
	public abstract T parse(HTTPResponse response) throws ResultException;
}
