package net.nowtryz.cafetapi.parsers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.nowtryz.cafetapi.datas.CafetData;
import net.nowtryz.cafetapi.exceptions.IllegalResultException;
import net.nowtryz.cafetapi.exceptions.MissingResultElementException;
import net.nowtryz.cafetapi.http.HTTPResponse;
import net.nowtryz.cafetapi.results.DataResult;
import net.nowtryz.cafetapi.results.Status;

/**
 * 
 * @author Damien
 * @since API 1.0 (2018)
 * @param <S>
 */
public class DataParser<S extends CafetData> extends AbstractParser<DataResult<S>> {
	private Class<S> classOfS;
	
	public DataParser(Class<S> classOfS) {
		this.classOfS = classOfS;
	}

	@Override
	public DataResult<S> parse(HTTPResponse response) throws MissingResultElementException, IllegalResultException {
		JsonElement json = this.parseJson(response.getResponse());
		JsonObject result;
		S parsed;

		if(!json.isJsonObject())
			throw new IllegalResultException("Unable to read the result : the result isn't an object");
		
		result = json.getAsJsonObject();
		
		
		if(!result.get(TYPE).isJsonPrimitive() || !result.getAsJsonPrimitive(TYPE).isString()) throw new IllegalResultException("Expected type to be a string");
		
		parsed = (S) this.getClassDecoder().fromJson(result, classOfS);
		
		return new DataResult<S>(Status.OK, this.getComputing(response), parsed);
	}
	
	public static <T extends CafetData> DataResult<T> parseResponse(HTTPResponse response, Class<T> classOfT) throws MissingResultElementException, IllegalResultException {
		return new DataParser<T>(classOfT).parse(response);
	}
}
