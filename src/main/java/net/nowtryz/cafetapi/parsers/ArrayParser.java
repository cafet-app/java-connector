package net.nowtryz.cafetapi.parsers;

import java.lang.reflect.Array;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.nowtryz.cafetapi.datas.CafetData;
import net.nowtryz.cafetapi.exceptions.IllegalResultException;
import net.nowtryz.cafetapi.exceptions.MissingResultElementException;
import net.nowtryz.cafetapi.http.HTTPResponse;
import net.nowtryz.cafetapi.results.ArrayResult;
import net.nowtryz.cafetapi.results.Status;

/**
 * @author Damien
 * @param <T>
 * @since API 1.0 (2018)
 */
public class ArrayParser<T extends CafetData> extends AbstractParser<ArrayResult<T>> {
	private Class<T> classOfT;
	private Class<T[]> classOfTArray;
	
	public ArrayParser(Class<T> classOfT, Class<T[]> classOfTArray) {
		this.classOfT = classOfT;
		this.classOfTArray = classOfTArray;
	}
	
	public static <T> T[] newArray(Class<T[]> type, int size) {
		   return type.cast(Array.newInstance(type.getComponentType(), size));
	}

	@Override
	public ArrayResult<T> parse(HTTPResponse response) throws MissingResultElementException, IllegalResultException {
		JsonElement json = this.parseJson(response.getResponse());
		JsonArray array;
		T[] resultArray;
		String type;
		Iterator<JsonElement> iterator;
		
		if(!json.isJsonArray())
			throw new IllegalResultException("Unable to read the result : the result isn't an array");
		
		array       = json.getAsJsonArray();
		resultArray = newArray(classOfTArray, array.size());
		
		
		if(array.size() > 0) {
			type        = array.get(0).getAsJsonObject().get(TYPE).getAsString();
			iterator    = array.iterator();
			
			for(int i = 0; iterator.hasNext(); i++) {
				JsonObject o = iterator.next().getAsJsonObject();
				
				if(!( o.has(TYPE) && o.get(TYPE).isJsonPrimitive() && o.getAsJsonPrimitive(TYPE).isString() )) throw new IllegalResultException("Expected type to be a string");
				
				if(!o.getAsJsonPrimitive(TYPE).getAsString().equals(type)) throw new IllegalResultException("Type mismatche between objects of the same array");
				
				resultArray[i] = (T) this.getClassDecoder().fromJson(o, classOfT);
			}
		}
		
		
		return new ArrayResult<T>(Status.OK, this.getComputing(response), resultArray);
	}
	
	public static <T extends CafetData> ArrayResult<T> parseResponse(HTTPResponse response, Class<T> classOfT, Class<T[]> classOfTArray) throws MissingResultElementException, IllegalResultException {
		return new ArrayParser<T>(classOfT, classOfTArray).parse(response);
	}
}
