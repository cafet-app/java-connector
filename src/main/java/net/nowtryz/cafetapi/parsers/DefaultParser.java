package net.nowtryz.cafetapi.parsers;

import net.nowtryz.cafetapi.exceptions.ResultException;
import net.nowtryz.cafetapi.http.HTTPResponse;
import net.nowtryz.cafetapi.results.Result;
import net.nowtryz.cafetapi.results.Status;

public class DefaultParser extends AbstractParser<Result> {

	@Override
	public Result parse(HTTPResponse response) throws ResultException {
		if(response.getCode()/100 < 3) {
			return new Result(Status.OK, this.getComputing(response));
		} else {
			return new Result(Status.ERROR, this.getComputing(response));
		}
	}
	
	public static Result parseResponse(HTTPResponse response) throws ResultException {
		return new DefaultParser().parse(response);
	}
}
