package net.nowtryz.cafetapi.parsers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.nowtryz.cafetapi.exceptions.IllegalResultException;
import net.nowtryz.cafetapi.exceptions.MissingResultElementException;
import net.nowtryz.cafetapi.http.HTTPResponse;
import net.nowtryz.cafetapi.results.LoginResult;
import net.nowtryz.cafetapi.results.Status;
import net.nowtryz.cafetapi.user.User;

/**
 * @author Damien
 * @since API 1.0 (2018)
 */
public class LoginParser extends AbstractParser<LoginResult> {
	private static final String USER = "user";
	private static final String SESSION_ID = "session";
	
	@Override
	public LoginResult parse(HTTPResponse response) throws MissingResultElementException, IllegalResultException {
		JsonElement json = parseJson(response.getResponse());
		String sessionId;
		JsonObject result;
		User user;

		
		if(!json.isJsonObject()) throw new IllegalResultException("Unable to read the result : the result isn't an object");
		
		result = json.getAsJsonObject();
		
		if(!result.has(USER)) throw new MissingResultElementException("Missing user field");
		if(!result.has(SESSION_ID)) throw new MissingResultElementException("Missing session field");
		
		if(!result.get(USER).isJsonObject() || !result.get(SESSION_ID).isJsonPrimitive() || !result.getAsJsonPrimitive(SESSION_ID).isString()) {
			throw new IllegalResultException("the result doesn't follow the LoginResult structure");
		}
		
		
		sessionId = result.getAsJsonPrimitive(SESSION_ID).getAsString();
		user = (User) this.getClassDecoder().fromJson(result.getAsJsonObject(USER), User.class);
		
		return new LoginResult(Status.OK, this.getComputing(response), sessionId, user);
	}
	
	public static LoginResult parseResponse(HTTPResponse response) throws MissingResultElementException, IllegalResultException {
		return new LoginParser().parse(response);
	}
}
