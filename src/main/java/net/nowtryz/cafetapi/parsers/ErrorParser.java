package net.nowtryz.cafetapi.parsers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.nowtryz.cafetapi.exceptions.IllegalResultException;
import net.nowtryz.cafetapi.http.HTTPResponse;
import net.nowtryz.cafetapi.results.ErrorResult;
import net.nowtryz.cafetapi.results.Status;

/**
 * @author Damien
 * @since API 1.0 (2018)
 */
public class ErrorParser extends AbstractParser<ErrorResult> {
	private static final String ERR_TYPE = "error_type";
	private static final String ERR_CODE = "error_code";
	private static final String ERR_MSG = "error_message";
	private static final String ERR_ADD_MSG = "additional_message";
	
	
	/**
	 * 
	 * @param response
	 * @param json
	 * @return ErrorResult the parsed result
	 * @throws IllegalResultException
	 * @since API 1.0 (2018)
	 */
	public ErrorResult parse(HTTPResponse response) throws IllegalResultException {
		JsonElement json = parseJson(response.getResponse());
		JsonObject error;
		
		if(!json.isJsonObject()) throw new IllegalResultException("Expected reponse to be a json object");
		
		error = json.getAsJsonObject();
						
		if(!error.has(ERR_CODE)) throw new IllegalResultException("Missing " + ERR_CODE);
		if(!error.has(ERR_TYPE)) throw new IllegalResultException("missing " + ERR_TYPE);
		if(!error.has(ERR_MSG)) throw new IllegalResultException("missing " + ERR_MSG);
		if(!error.get(ERR_CODE).isJsonPrimitive() || !error.getAsJsonPrimitive(ERR_CODE).isString()) throw new IllegalResultException("Expected " + ERR_CODE + " to be a string");
		if(!error.get(ERR_TYPE).isJsonPrimitive() || !error.getAsJsonPrimitive(ERR_TYPE).isString()) throw new IllegalResultException("Expected " + ERR_TYPE + " to be a string");
		if(!error.get(ERR_MSG).isJsonPrimitive() || !error.getAsJsonPrimitive(ERR_MSG).isString()) throw new IllegalResultException("Expected " + ERR_MSG + " to be a string");
		if(error.has(ERR_ADD_MSG) && (!error.get(ERR_ADD_MSG).isJsonPrimitive() || !error.getAsJsonPrimitive(ERR_ADD_MSG).isString())) throw new IllegalResultException("Expected " + ERR_ADD_MSG + " to be a string");
		
		return new ErrorResult(
				Status.ERROR, 
				getComputing(response), 
				error.getAsJsonPrimitive(ERR_CODE).getAsString(),
				error.getAsJsonPrimitive(ERR_TYPE).getAsString(),
				error.getAsJsonPrimitive(ERR_MSG).getAsString(),
				error.has(ERR_ADD_MSG) ? error.getAsJsonPrimitive(ERR_ADD_MSG).getAsString() : null
				);
	}
	
	public static ErrorResult parseResponse(HTTPResponse response) throws IllegalResultException {
		return new ErrorParser().parse(response);
	}
}
