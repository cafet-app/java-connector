package net.nowtryz.cafetapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareEverythingForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import net.nowtryz.cafetapi.exceptions.APIException;
import net.nowtryz.cafetapi.exceptions.BannedUserException;
import net.nowtryz.cafetapi.exceptions.FailedAuthenticationException;
import net.nowtryz.cafetapi.exceptions.HTTPException;
import net.nowtryz.cafetapi.exceptions.PermissionException;
import net.nowtryz.cafetapi.exceptions.ServerErrorException;
import net.nowtryz.cafetapi.http.HTTPResponse;
import net.nowtryz.cafetapi.http.HttpManager;
import net.nowtryz.cafetapi.http.HttpManager.HttpMethod;
import net.nowtryz.cafetapi.parsers.LoginParser;
import net.nowtryz.cafetapi.results.LoginResult;
import net.nowtryz.cafetapi.user.User;

@RunWith(PowerMockRunner.class)
//@PrepareForTest({
//	HttpManager.class,
//	LoginParser.class,
//	HashMap.class,
//	Base64.class,
//	Base64.Encoder.class,
//	ServerExceptionsManager.class
//})
/*
 * FIXME preparation for tests don't enable static moks of Base64.getEncoder() and new HashMap<String, String>()
 * mocks are not availabe in the tested class, while available in the current class (UtCafetAPILogin)
 */
@PrepareEverythingForTest
@PowerMockIgnore({"org.mockito.*"})
public class UtCafetAPILogin {
	public static final String email = "myemail@domain.com";
	public static final String password = "password";
	public static final String sessionId = "session";
	public static final String authToken = "token";
	public static final String resultString = "response string";
	public static URL url;

	private CafetAPI api = null;

	//mocks
	@Mock private HTTPResponse response;
	@Mock private LoginResult result;
	@Mock private HttpManager manager;
	@Mock private Encoder encoder;
	@Mock private HashMap<String, String> headers;

	private User user;

	@BeforeClass
	public static void app() throws MalformedURLException {
		url = new URL(UtCafetAPI.URL);
	}

	@Before
	public void before() throws Exception {
		mockStatic(HttpManager.class);
		mockStatic(LoginParser.class);
		mockStatic(HashMap.class);
		mockStatic(Base64.class);
		when(HttpManager.newInstance(url)).thenReturn(manager);
		when(Base64.getEncoder()).thenReturn(encoder);
		whenNew(HashMap.class).withNoArguments().thenReturn(headers);

		api = new CafetAPI(url);

		user = new User();

		//inner affectations

		//returns mocking
		when(encoder.encodeToString(any(byte[].class))).thenReturn(authToken);
		when(result.getSessionId()).thenReturn(sessionId);
		when(result.getUser()).thenReturn(user);
	}
	
	@After
	public void after() throws Exception {
		verify(headers).put("Authorization", "Basic " + authToken);
		verify(manager).query(eq(HttpMethod.POST), eq("user/login"), isNull(), any());
	}

	@Test
	public void testLogin200() throws Exception {
		when(LoginParser.parseResponse(response)).thenReturn(result);
		when(manager.query(eq(HttpMethod.POST), eq("user/login"), isNull(), any())).thenReturn(response);

		// tests
		assertEquals(result, api.login(email, password));
		assertEquals(user, api.getUser());

		//verifications
		verify(result).getSessionId();
		verify(result).getUser();
		verify(manager).setSessionId(sessionId);
	}

	private <T extends APIException> void errorLogin(Class<T> errorClass) throws Exception {

		HTTPException httpException = mock(HTTPException.class);
		T exception = mock(errorClass);
		
		mockStatic(ServerExceptionsManager.class);
		when(ServerExceptionsManager.handle(eq(httpException))).thenReturn(exception);

		when(manager.query(eq(HttpMethod.POST), eq("user/login"), isNull(), any())).thenThrow(httpException);

		Throwable thrownException = assertThrows(errorClass, () -> {
			api.login(email, password);
		});

		assertEquals(exception, thrownException);
	}

	@Test
	public void testLogin02_001() throws Exception {
		errorLogin(FailedAuthenticationException.class);
	}

	@Test
	public void testLogin02_002() throws Exception {
		errorLogin(BannedUserException.class);
	}

	@Test
	public void testLogin02_003() throws Exception {
		errorLogin(PermissionException.class);
	}

	@Test
	public void testLoginDefaultException() throws Exception {
		errorLogin(ServerErrorException.class);
	}

}
