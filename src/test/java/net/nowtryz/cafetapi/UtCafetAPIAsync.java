package net.nowtryz.cafetapi;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import net.nowtryz.cafetapi.AsyncHandler.AsyncArrayHandler;
import net.nowtryz.cafetapi.AsyncHandler.AsyncDataHandler;
import net.nowtryz.cafetapi.AsyncHandler.AsyncLoginHandler;
import net.nowtryz.cafetapi.datas.Client;
import net.nowtryz.cafetapi.datas.Expense;
import net.nowtryz.cafetapi.datas.ExpenseDetail;
import net.nowtryz.cafetapi.datas.ProductBought;
import net.nowtryz.cafetapi.datas.Reload;
import net.nowtryz.cafetapi.exceptions.APIException;
import net.nowtryz.cafetapi.results.ArrayResult;
import net.nowtryz.cafetapi.results.DataResult;
import net.nowtryz.cafetapi.results.LoginResult;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CafetAPI.class)
public class UtCafetAPIAsync {
	public static URL url;

	//Mocks
	@Mock private CafetAPI api;
	@Mock private Thread thread;

	private ArgumentCaptor<Runnable> runnables = ArgumentCaptor.forClass(Runnable.class);

	@BeforeClass
	public static void beforeClass() throws MalformedURLException {
		url = new URL(UtCafetAPI.URL);
	}

	@Before
	public void beforeEach() throws Exception {
		doAnswer(InvocationOnMock::callRealMethod).when(api).asyncLogin(
			any(AsyncLoginHandler.class), anyString(), anyString()
		);
		doAnswer(InvocationOnMock::callRealMethod).when(api).asyncGetClients(
			any(Utils.<AsyncArrayHandler<Client>>castClass(AsyncArrayHandler.class))
		);
		doAnswer(InvocationOnMock::callRealMethod).when(api).asyncGetClient(
			any(Utils.<AsyncDataHandler<Client>>castClass(AsyncDataHandler.class)), anyInt()
		);
		doAnswer(InvocationOnMock::callRealMethod).when(api).asyncGetClientReloads(
			any(Utils.<AsyncArrayHandler<Reload>>castClass(AsyncArrayHandler.class)), anyInt()
		);
		doAnswer(InvocationOnMock::callRealMethod).when(api).asyncGetClientExpenses(
			any(Utils.<AsyncArrayHandler<Expense>>castClass(AsyncArrayHandler.class)), anyInt()
		);
		doAnswer(InvocationOnMock::callRealMethod).when(api).asyncGetExpenseDetails(
			any(Utils.<AsyncArrayHandler<ExpenseDetail>>castClass(AsyncArrayHandler.class)), anyInt()
		);
		doAnswer(InvocationOnMock::callRealMethod).when(api).asyncGetProductBought(
			any(Utils.<AsyncDataHandler<ProductBought>>castClass(AsyncDataHandler.class)), anyInt()
		);

		whenNew(Thread.class)
		.withParameterTypes(Runnable.class, String.class)
		.withArguments(runnables.capture(), anyString())
		.thenReturn(thread);
	}

	@Test
	public void asyncLoginOK() throws Exception {
		final String email = "email";
		final String password = "password";
		final AsyncLoginHandler handler = mock(AsyncLoginHandler.class);
		final LoginResult result = mock(LoginResult.class);

		// OK
		when(api.login(email, password)).thenReturn(result);
		api.asyncLogin(handler, email, password);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleResult(result);
		verify(api, times(1)).login(email, password);
	}

	@Test
	public void asyncLoginAPI() throws Exception {
		final String email = "email";
		final String password = "password";
		final AsyncLoginHandler handler = mock(AsyncLoginHandler.class);
		final APIException ex = mock(APIException.class);

		// API Exception
		when(api.login(email, password)).thenThrow(ex);
		api.asyncLogin(handler, email, password);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).login(email, password);
	}

	@Test
	public void asyncLoginIO() throws Exception {
		final String email = "email";
		final String password = "password";
		final AsyncLoginHandler handler = mock(AsyncLoginHandler.class);
		final IOException ex = mock(IOException.class);

		// IO Exception
		when(api.login(email, password)).thenThrow(ex);
		api.asyncLogin(handler, email, password);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).login(email, password);
	}

	@Test
	public void asyncGetClientsOK() throws Exception {
		final AsyncArrayHandler<Client> handler = mock(Utils.<AsyncArrayHandler<Client>>castClass(AsyncArrayHandler.class));
		final ArrayResult<Client> result = mock(Utils.<ArrayResult<Client>>castClass(ArrayResult.class));

		// OK
		when(api.getClients()).thenReturn(result);
		api.asyncGetClients(handler);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleResult(result);
		verify(api, times(1)).getClients();
	}

	@Test
	public void asyncGetClientsAPI() throws Exception {
		final AsyncArrayHandler<Client> handler = mock(Utils.<AsyncArrayHandler<Client>>castClass(AsyncArrayHandler.class));
		final APIException ex = mock(APIException.class);

		// API Exception
		when(api.getClients()).thenThrow(ex);
		api.asyncGetClients(handler);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getClients();
	}

	@Test
	public void asyncGetClientsIO() throws Exception {
		final AsyncArrayHandler<Client> handler = mock(Utils.<AsyncArrayHandler<Client>>castClass(AsyncArrayHandler.class));
		final IOException ex = mock(IOException.class);

		// API Exception
		when(api.getClients()).thenThrow(ex);
		api.asyncGetClients(handler);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getClients();
	}

	@Test
	public void asyncGetClientOK() throws Exception {
		final int id = 0;
		final AsyncDataHandler<Client> handler = mock(Utils.<AsyncDataHandler<Client>>castClass(AsyncDataHandler.class));
		final DataResult<Client> result = mock(Utils.<DataResult<Client>>castClass(DataResult.class));

		// OK
		when(api.getClient(id)).thenReturn(result);
		api.asyncGetClient(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleResult(result);
		verify(api, times(1)).getClient(id);
	}

	@Test
	public void asyncGetClientAPI() throws Exception {
		final int id = 0;
		final AsyncDataHandler<Client> handler = mock(Utils.<AsyncDataHandler<Client>>castClass(AsyncDataHandler.class));
		final APIException ex = mock(APIException.class);

		// API Exception
		when(api.getClient(id)).thenThrow(ex);
		api.asyncGetClient(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getClient(id);
	}

	@Test
	public void asyncGetClientIO() throws Exception {
		final int id = 0;
		final AsyncDataHandler<Client> handler = mock(Utils.<AsyncDataHandler<Client>>castClass(AsyncDataHandler.class));
		final IOException ex = mock(IOException.class);

		// API Exception
		when(api.getClient(id)).thenThrow(ex);
		api.asyncGetClient(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getClient(id);
	}

	@Test
	public void asyncGetClientReloadsOK() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<Reload> handler = mock(Utils.<AsyncArrayHandler<Reload>>castClass(AsyncArrayHandler.class));
		final ArrayResult<Reload> result = mock(Utils.<ArrayResult<Reload>>castClass(ArrayResult.class));

		// OK
		when(api.getClientReloads(id)).thenReturn(result);
		api.asyncGetClientReloads(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleResult(result);
		verify(api, times(1)).getClientReloads(id);
	}

	@Test
	public void asyncGetClientReloadsAPI() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<Reload> handler = mock(Utils.<AsyncArrayHandler<Reload>>castClass(AsyncArrayHandler.class));
		final APIException ex = mock(APIException.class);

		// API Exception
		when(api.getClientReloads(id)).thenThrow(ex);
		api.asyncGetClientReloads(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getClientReloads(id);
	}

	@Test
	public void asyncGetClientReloadsIO() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<Reload> handler = mock(Utils.<AsyncArrayHandler<Reload>>castClass(AsyncArrayHandler.class));
		final IOException ex = mock(IOException.class);

		// API Exception
		when(api.getClientReloads(id)).thenThrow(ex);
		api.asyncGetClientReloads(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getClientReloads(id);
	}

	@Test
	public void asyncGetClientExpensesOK() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<Expense> handler = mock(Utils.<AsyncArrayHandler<Expense>>castClass(AsyncArrayHandler.class));
		final ArrayResult<Expense> result = mock(Utils.<ArrayResult<Expense>>castClass(ArrayResult.class));

		// OK
		when(api.getClientExpenses(id)).thenReturn(result);
		api.asyncGetClientExpenses(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleResult(result);
		verify(api, times(1)).getClientExpenses(id);
	}

	@Test
	public void asyncGetClientExpensesAPI() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<Expense> handler = mock(Utils.<AsyncArrayHandler<Expense>>castClass(AsyncArrayHandler.class));
		final APIException ex = mock(APIException.class);

		// API Exception
		when(api.getClientExpenses(id)).thenThrow(ex);
		api.asyncGetClientExpenses(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getClientExpenses(id);
	}

	@Test
	public void asyncGetClientExpensesIO() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<Expense> handler = mock(Utils.<AsyncArrayHandler<Expense>>castClass(AsyncArrayHandler.class));
		final IOException ex = mock(IOException.class);

		// API Exception
		when(api.getClientExpenses(id)).thenThrow(ex);
		api.asyncGetClientExpenses(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getClientExpenses(id);
	}

	@Test
	public void asyncGetExpenseDetailsOK() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<ExpenseDetail> handler = mock(Utils.<AsyncArrayHandler<ExpenseDetail>>castClass(AsyncArrayHandler.class));
		final ArrayResult<ExpenseDetail> result = mock(Utils.<ArrayResult<ExpenseDetail>>castClass(ArrayResult.class));

		// OK
		when(api.getExpenseDetails(id)).thenReturn(result);
		api.asyncGetExpenseDetails(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleResult(result);
		verify(api, times(1)).getExpenseDetails(id);
	}

	@Test
	public void asyncGetExpenseDetailsAPI() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<ExpenseDetail> handler = mock(Utils.<AsyncArrayHandler<ExpenseDetail>>castClass(AsyncArrayHandler.class));
		final APIException ex = mock(APIException.class);

		// API Exception
		when(api.getExpenseDetails(id)).thenThrow(ex);
		api.asyncGetExpenseDetails(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getExpenseDetails(id);
	}

	@Test
	public void asyncGetExpenseDetailsIO() throws Exception {
		final int id = 0;
		final AsyncArrayHandler<ExpenseDetail> handler = mock(Utils.<AsyncArrayHandler<ExpenseDetail>>castClass(AsyncArrayHandler.class));
		final IOException ex = mock(IOException.class);

		// API Exception
		when(api.getExpenseDetails(id)).thenThrow(ex);
		api.asyncGetExpenseDetails(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getExpenseDetails(id);
	}

	@Test
	public void asyncGetProductBoughtOK() throws Exception {
		final int id = 0;
		final AsyncDataHandler<ProductBought> handler = mock(Utils.<AsyncDataHandler<ProductBought>>castClass(AsyncDataHandler.class));
		final DataResult<ProductBought> result = mock(Utils.<DataResult<ProductBought>>castClass(DataResult.class));

		// OK
		when(api.getProductBought(id)).thenReturn(result);
		api.asyncGetProductBought(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleResult(result);
		verify(api, times(1)).getProductBought(id);
	}

	@Test
	public void asyncGetProductBoughtAPI() throws Exception {
		final int id = 0;
		final AsyncDataHandler<ProductBought> handler = mock(Utils.<AsyncDataHandler<ProductBought>>castClass(AsyncDataHandler.class));
		final APIException ex = mock(APIException.class);

		// API Exception
		when(api.getProductBought(id)).thenThrow(ex);
		api.asyncGetProductBought(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getProductBought(id);
	}

	@Test
	public void asyncGetProductBoughtIO() throws Exception {
		final int id = 0;
		final AsyncDataHandler<ProductBought> handler = mock(Utils.<AsyncDataHandler<ProductBought>>castClass(AsyncDataHandler.class));
		final IOException ex = mock(IOException.class);

		// API Exception
		when(api.getProductBought(id)).thenThrow(ex);
		api.asyncGetProductBought(handler, id);
		runnables.getValue().run();

		//verifications
		verify(handler, times(1)).handleException(ex);
		verify(api, times(1)).getProductBought(id);
	}
}
