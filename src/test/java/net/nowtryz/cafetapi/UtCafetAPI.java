package net.nowtryz.cafetapi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.nowtryz.cafetapi.user.User;;

public class UtCafetAPI {
	public static final String URL = "http://localhost/";
	public static URL url;
    private CafetAPI api = null;
    
    @SuppressWarnings("unchecked")
    private <T> T getPrivateField(CafetAPI api, String fieldName, Class<T> fieldClass) throws Exception {
        Field field = CafetAPI.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        return (T) field.get(api);
    }

    private <T> void setPrivateField(CafetAPI api, String fieldName, T value) throws Exception {
        Field field = CafetAPI.class.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(api, value);
    }
	
	@BeforeAll
	public static void app() throws MalformedURLException {
		UtCafetAPI.url = new URL(URL);
	}
	
	@BeforeEach
	public void before() {
		api = new CafetAPI(url);
	}

    @Test
    public void testGetVersion() throws Exception {
        String version = getPrivateField(null, "SERVER_API_VERSION", String.class);
        assertEquals(version, CafetAPI.getVersion());
    }

    @Test
    public void testGetInstance() throws Exception {
        CafetAPI instance = getPrivateField(null, "instance", CafetAPI.class);
        assertEquals(instance, CafetAPI.getInstance());
    }

    @Test
    public void testGetServerVersion() throws Exception {
        String serverVersion = "serverVersion";
        setPrivateField(api, "serverVersion", serverVersion);
        assertEquals(serverVersion, api.getServerVersion());
    }

    @Test
    public void testGetUser() throws Exception {
        User user = new User();
        setPrivateField(api, "user", user);
        assertEquals(user, api.getUser());
    }

    @Test
    public void testSetInstance() throws Exception {
        api = new CafetAPI(url);
        CafetAPI.setInstance(api);
        assertEquals(api, CafetAPI.getInstance());
    }

    @Test
    public void testConstructor() throws Exception {

    }

}